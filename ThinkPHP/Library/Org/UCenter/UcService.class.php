<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
namespace Org\UCenter;
class UcService {

    public function __construct() {
        include_once(ADMEXP_ROOT_PATH . 'Application/Home/Conf/config_ucenter.php');
        include_once(ADMEXP_ROOT_PATH . 'uc_client/client.php');
    }

    /**
     * 会员注册
     */
    public function register($username, $password, $email) {
        $uid = uc_user_register($username, $password, $email); //UCenter的注册验证函数
        if ($uid <= 0) {
            if ($uid == -1) {
                return '用户名不合法';
            } elseif ($uid == -2) {
                return '包含不允许注册的词语';
            } elseif ($uid == -3) {
                return '用户名已经存在';
            } elseif ($uid == -4) {
                return 'Email 格式有误';
            } elseif ($uid == -5) {
                return 'Email 不允许注册';
            } elseif ($uid == -6) {
                return '该 Email 已经被注册';
            } else {
                return '未定义';
            }
        } else {
            return intval($uid); //返回一个非负数
        }
    }
    
    /**
     * 会员登录
     */
    public function login($username,$password){
        list($uid, $username, $password, $email) = uc_user_login($username,$password);
        $data['uid']=$uid;
        $data['username']=$username;
        $data['password']=$password;
        $data['email']=$email;      
        
        if($uid > 0) {
            $data['msg']="登录成功";
             $data['synhtml']= uc_user_synlogin($uid);
        } elseif($uid == -1) {
            $data['msg']="用户不存在,或者被删除";                 
        } elseif($uid == -2) {
            $data['msg']="密码错";              
        } else {
              $data['msg']="未定义";              
        }     
        return $data;
    }
    
    /**
     * 会员退出
     */
    public function logout(){
        return uc_user_synlogout();
    }
}
