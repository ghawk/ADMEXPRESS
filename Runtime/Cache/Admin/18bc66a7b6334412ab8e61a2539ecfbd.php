<?php if (!defined('THINK_PATH')) exit();?><!doctype html>
<html>
    <head>
        <meta charset="UTF-8">
        <title><?php echo ($meta_title); ?>|ADMEXPRESS管理平台</title>
        <link href="/admexp/Public/favicon.ico" type="image/x-icon" rel="shortcut icon">
       
        <link rel="stylesheet" type="text/css" href="/admexp/Public/Admin/css/base.css" media="all">
        <link rel="stylesheet" type="text/css" href="/admexp/Public/Admin/css/common.css" media="all">
        <link rel="stylesheet" type="text/css" href="/admexp/Public/Admin/css/module.css">
        <link rel="stylesheet" type="text/css" href="/admexp/Public/Admin/css/style.css" media="all">
        <link rel="stylesheet" type="text/css" href="/admexp/Public/Admin/css/<?php echo (C("COLOR_STYLE")); ?>.css" media="all">     
        <!--
            <link rel="stylesheet" type="text/css" href="/admexp/Public/static/font-awesome/css/font-awesome.min.css" media="all">
        -->
        <!--[if lt IE 9]>
       <script type="text/javascript" src="/admexp/Public/static/jquery-1.10.2.min.js"></script>
       <![endif]--><!--[if gte IE 9]><!-->
        <script type="text/javascript" src="/admexp/Public/static/jquery-2.0.3.min.js"></script>

        <script type="text/javascript" src="/admexp/Public/Admin/js/jquery.mousewheel.js"></script>
        <!--<![endif]-->
    
</head>
<body>
    <!-- 头部 -->
    <div class="header">
        <!-- Logo -->
        <span class="logo"></span>
        <!-- /Logo -->

        <!-- 主导航 -->
        <ul class="main-nav">
            <?php if(is_array($__MENU__["main"])): $i = 0; $__LIST__ = $__MENU__["main"];if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$menu): $mod = ($i % 2 );++$i;?><li class="<?php echo ((isset($menu["class"]) && ($menu["class"] !== ""))?($menu["class"]):''); ?>"><a href="<?php echo (u($menu["url"])); ?>"><?php echo ($menu["title"]); ?></a></li><?php endforeach; endif; else: echo "" ;endif; ?>
        </ul>
        <!-- /主导航 -->

        <!-- 用户栏 -->
        <div class="user-bar">
            <a href="javascript:;" class="user-entrance"><i class="icon-user"></i></a>
            <ul class="nav-list user-menu hidden">
                <li class="manager">你好，<em title="<?php echo session('user_auth.username');?>"><?php echo session('user_auth.username');?></em></li>
                <li><a href="<?php echo U('User/updatePassword');?>">修改密码</a></li>
                <li><a href="<?php echo U('User/updateNickname');?>">修改昵称</a></li>
                <li><a href="<?php echo U('Public/logout');?>">退出</a></li>
            </ul>
        </div>
    </div>
    <!-- /头部 -->

    <!-- 边栏 -->
    <div class="sidebar">
        <!-- 子导航 -->
        
            <div id="subnav" class="subnav">
                <?php if(!empty($_extra_menu)): ?>
                    <?php echo extra_menu($_extra_menu,$__MENU__); endif; ?>
                <?php if(is_array($__MENU__["child"])): $i = 0; $__LIST__ = $__MENU__["child"];if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$sub_menu): $mod = ($i % 2 );++$i;?><!-- 子导航 -->
                    <?php if(!empty($sub_menu)): if(!empty($key)): ?><h3><i class="icon icon-unfold"></i><?php echo ($key); ?></h3><?php endif; ?>
                        <ul class="side-sub-menu">
                            <?php if(is_array($sub_menu)): $i = 0; $__LIST__ = $sub_menu;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$menu): $mod = ($i % 2 );++$i;?><li>
                                    <a class="item" href="<?php echo (u($menu["url"])); ?>"><?php echo ($menu["title"]); ?></a>
                                </li><?php endforeach; endif; else: echo "" ;endif; ?>
                        </ul><?php endif; ?>
                    <!-- /子导航 --><?php endforeach; endif; else: echo "" ;endif; ?>
            </div>
        
        <!-- /子导航 -->
    </div>
    <!-- /边栏 -->

    <!-- 内容区 -->
    <div id="main-content">
        <div id="top-alert" class="fixed alert alert-error" style="display: none;">
            <button class="close fixed" style="margin-top: 4px;">&times;</button>
            <div class="alert-content">这是内容</div>
        </div>
        <div id="main" class="main">
            
                <!-- nav -->
                <?php if(!empty($_show_nav)): ?><div class="breadcrumb">
                        <span>您的位置:</span>
                        <?php $i = '1'; ?>
                        <?php if(is_array($_nav)): foreach($_nav as $k=>$v): if($i == count($_nav)): ?><span><?php echo ($v); ?></span>
                                <?php else: ?>
                                <span><a href="<?php echo ($k); ?>"><?php echo ($v); ?></a>&gt;</span><?php endif; ?>
                            <?php $i = $i+1; endforeach; endif; ?>
                    </div><?php endif; ?>
                <!-- nav -->
            

            
    <form action="<?php echo U('Order/packprint');?>" method="POST" target="_blank">
    <!-- 标题栏 -->
    <div class="main-title">
        <h2>订单管理
        [
        <?php if(($type) == "-1"): ?><strong>全部</strong><?php else: ?><a href="<?php echo U('index');?>">全部</a><?php endif; ?>
        <?php if(($type) == "3000"): ?><strong>打包称重</strong><?php else: ?><a href="<?php echo U('index?type=3000');?>">打包称重</a><?php endif; ?>
        <?php if(($type) == "4000"): ?><strong>等待付款</strong><?php else: ?><a href="<?php echo U('index?type=4000');?>">等待付款</a><?php endif; ?>
        <?php if(($type) == "5000"): ?><strong>等待发货</strong><?php else: ?><a href="<?php echo U('index?type=5000');?>">等待发货</a><?php endif; ?>
        <?php if(($type) == "6000"): ?><strong>运输途中</strong><?php else: ?><a href="<?php echo U('index?type=6000');?>">运输途中</a><?php endif; ?>
        <?php if(($type) == "7000"): ?><strong>签收完毕</strong><?php else: ?><a href="<?php echo U('index?type=7000');?>">签收完毕</a><?php endif; ?>
        ]
        </h2>
    </div>
    
    <div class="cf">
        <div class="fl">
            <input type="submit" value="批量打印" class="confirm  btn">
        </div>
        <!-- 高级搜索 -->
        <div class="search-form fr cf">
            <div class="sleft">
                     <input type="text" name="id" class="search-input"  placeholder="请输入订单编号查询订单">
                <input type="text" name="name" class="search-input" placeholder="请输入用户名查询订单">
                <a class="sch-btn" href="javascript:;" id="search" url="<?php echo U('index');?>"><i class="btn-search"></i></a>
            </div>
        </div>
    </div>
    
    <!-- 数据列表 -->
    
    <div class="data-table table-striped">
     
        <table class="">
            <thead>
                <tr>
                    <th class="row-selected row-selected"><input class="check-all" type="checkbox"/></th>
                    <th class="">OID</th>
                    <th class="">仓库名称</th>
                    <th class="">包含包裹</th>
                    <th class="">货主</th>
                    <th class="">收货地址</th>                    
                    <th class="">打包后包裹属性</th>       
                    <th class="">货运线路</th>  
                    <th class="">运单金额</th>
                    <th class="">创建日期</th>
                    <th class="">状态</th>
                    <th class="">操作</th>
                </tr>
            </thead>
            <tbody>
            <?php if(!empty($_list)): if(is_array($_list)): $i = 0; $__LIST__ = $_list;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$vo): $mod = ($i % 2 );++$i;?><tr>
                        <td><input class="ids" type="checkbox" name="oid[]" value="<?php echo ($vo["id"]); ?>" /></td>
                        <td><?php echo ($vo["id"]); ?> </td>	
                        <td><?php echo (get_warehouse_name($vo["wid"])); ?> </td>
                        <td>
                    <?php if(is_array($vo["packages"])): $i = 0; $__LIST__ = $vo["packages"];if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$vs): $mod = ($i % 2 );++$i;?>[<?php echo ($vs["id"]); ?>]<b><?php echo ($vs["name"]); ?></b> <span style="color: green;margin-left:10px; background-color:#EEC068;border-bottom: 1px solid #000;"><?php echo ($vs["location"]); ?></span>  <span style="color: red;border-bottom: 1px solid #000"><?php if(($vs["bulkfactor"]) > $vs["weight"]): echo ($vs["bulkfactor"]); else: echo ($vs["weight"]); endif; ?>g</span><br/>
                        <?php echo ($vs["remark"]); ?><br/>
                        <hr/><?php endforeach; endif; else: echo "" ;endif; ?>
                    </td>
                    <td><span class='icon-user'><?php echo (get_username($vo["uid"])); ?></span></td>
                    <td><strong><?php echo ($vo["name"]); ?></strong><br/><?php echo ($vo["country"]); ?>,<?php echo ($vo["province"]); ?>,<?php echo ($vo["city"]); ?>,<?php echo ($vo["address"]); ?>,<?php echo ($vo["zipcode"]); ?><br/><?php echo ($vo["phone"]); ?></td>
                    <td>秤重：<?php echo ($vo["weight"]); ?><br/>体积重：<?php echo ($vo["bulkfactor"]); ?><br/>存放位置：<?php echo ($vo["location"]); ?></td>
                    <td><span><?php echo (get_expline_name($vo["eid"])); ?></span></td>	
                    <td><?php if(($vo["money"]) > "0"): ?><span style="font-weight: bold;color:red"><?php echo ($vo["money"]); ?>元</span><?php else: ?>尚未计算<?php endif; ?></td>
                    <td><span><?php echo (time_format($vo["createdate"])); ?></span></td>		 
                    <td><?php echo ($vo["status"]); ?>|<?php echo ($vo["status_text"]); ?></td>
                    <td>  <a href="<?php echo U('Order/details?oid='.$vo['id'].'');?>" class="confirm  btn">订单管理</a></td>
                    </tr><?php endforeach; endif; else: echo "" ;endif; ?>
                <?php else: ?>
                <td colspan="12" class="text-center"> aOh! 暂时还没有内容! </td><?php endif; ?>
           
        </table>
         
    </div>
    <div class="page">
        <?php echo ($_page); ?>
    </div>
    </form>

        </div>
        <div class="cont-ft">
            <div class="copyright">
                <div class="fl">感谢使用<a href="http://www.admexpress.cn" target="_blank">ADMEXPRESS</a>管理平台</div>
                <div class="fr">V<?php echo (ADMEXPRESS_VERSION); ?></div>
            </div>
        </div>
    </div>
    <!-- /内容区 -->
    <script type="text/javascript">
        (function() {
            var ThinkPHP = window.Think = {
                "ROOT": "/admexp", //当前网站地址
                "APP": "/admexp/index.php", //当前项目地址
                "PUBLIC": "/admexp/Public", //项目公共目录地址
                "DEEP": "<?php echo C('URL_PATHINFO_DEPR');?>", //PATHINFO分割符
                "MODEL": ["<?php echo C('URL_MODEL');?>", "<?php echo C('URL_CASE_INSENSITIVE');?>", "<?php echo C('URL_HTML_SUFFIX');?>"],
                "VAR": ["<?php echo C('VAR_MODULE');?>", "<?php echo C('VAR_CONTROLLER');?>", "<?php echo C('VAR_ACTION');?>"]
            }
        })();
    </script>
    <script type="text/javascript" src="/admexp/Public/static/think.js"></script>
    <script type="text/javascript" src="/admexp/Public/Admin/js/common.js"></script>
    <script type="text/javascript">
        +function() {
            var $window = $(window), $subnav = $("#subnav"), url;
            $window.resize(function() {
                $("#main").css("min-height", $window.height() - 130);
            }).resize();

            /* 左边菜单高亮 */
            url = window.location.pathname + window.location.search;
            url = url.replace(/(\/(p)\/\d+)|(&p=\d+)|(\/(id)\/\d+)|(&id=\d+)|(\/(group)\/\d+)|(&group=\d+)/, "");
            $subnav.find("a[href='" + url + "']").parent().addClass("current");

            /* 左边菜单显示收起 */
            $("#subnav").on("click", "h3", function() {
                var $this = $(this);
                $this.find(".icon").toggleClass("icon-fold");
                $this.next().slideToggle("fast").siblings(".side-sub-menu:visible").
                        prev("h3").find("i").addClass("icon-fold").end().end().hide();
            });

            $("#subnav h3 a").click(function(e) {
                e.stopPropagation()
            });

            /* 头部管理员菜单 */
            $(".user-bar").mouseenter(function() {
                var userMenu = $(this).children(".user-menu ");
                userMenu.removeClass("hidden");
                clearTimeout(userMenu.data("timeout"));
            }).mouseleave(function() {
                var userMenu = $(this).children(".user-menu");
                userMenu.data("timeout") && clearTimeout(userMenu.data("timeout"));
                userMenu.data("timeout", setTimeout(function() {
                    userMenu.addClass("hidden")
                }, 100));
            });

            /* 表单获取焦点变色 */
            $("form").on("focus", "input", function() {
                $(this).addClass('focus');
            }).on("blur", "input", function() {
                $(this).removeClass('focus');
            });
            $("form").on("focus", "textarea", function() {
                $(this).closest('label').addClass('focus');
            }).on("blur", "textarea", function() {
                $(this).closest('label').removeClass('focus');
            });

            // 导航栏超出窗口高度后的模拟滚动条
            var sHeight = $(".sidebar").height();
            var subHeight = $(".subnav").height();
            var diff = subHeight - sHeight; //250
            var sub = $(".subnav");
            if (diff > 0) {
                $(window).mousewheel(function(event, delta) {
                    if (delta > 0) {
                        if (parseInt(sub.css('marginTop')) > -10) {
                            sub.css('marginTop', '0px');
                        } else {
                            sub.css('marginTop', '+=' + 10);
                        }
                    } else {
                        if (parseInt(sub.css('marginTop')) < '-' + (diff - 10)) {
                            sub.css('marginTop', '-' + (diff - 10));
                        } else {
                            sub.css('marginTop', '-=' + 10);
                        }
                    }
                });
            }
        }();
    </script>

    <script src="/admexp/Public/static/thinkbox/jquery.thinkbox.js"></script>

    <script type="text/javascript">
        //搜索功能
        $("#search").click(function() {
            var url = $(this).attr('url');
            var query = $('.search-form').find('input').serialize();
            query = query.replace(/(&|^)(\w*?\d*?\-*?_*?)*?=?((?=&)|(?=$))/g, '');
            query = query.replace(/^&/g, '');
            if (url.indexOf('?') > 0) {
                url += '&' + query;
            } else {
                url += '?' + query;
            }
            window.location.href = url;
        });
        //回车搜索
        $(".search-input").keyup(function(e) {
            if (e.keyCode === 13) {
                $("#search").click();
                return false;
            }
        });
//导航高亮
        highlight_subnav('<?php echo U('Order/index');?>');
    </script>

</body>
</html>