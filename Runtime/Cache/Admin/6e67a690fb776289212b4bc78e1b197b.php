<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <style>
        body{margin: 0px;padding: 0px;}
        .img2wei{margin: 2px;}
        .table2{border: 1px solid #000;}
        .table2 tr th{
            background-color: #F5F5F5;
            font-size: 9px;
            font-weight: normal;
            border: 1px solid #000;
            padding-left: 5px;
        }
        .table2 tr td{
            border: 1px solid #000;
            padding-left: 5px;
            font-weight: bold;
        }   
        .toolbarpr{
            padding:20px;
            border-bottom: 2px solid #006dcc;
            background-color: #091620;
            color: #fff;
        }

    </style>
    <head>
        <title>出库单打印</title>
        <link rel="stylesheet" type="text/css" href="/admexp/Public/static/bootstrapv3/css/bootstrap.min.css" media="all">
        <link rel="stylesheet" type="text/css" href="/admexp/Public/static/css/font-awesome.min.css" media="all">   
        <script type="text/javascript" src="/admexp/Public/static/jquery-2.0.3.min.js"></script> 
        <script src="/admexp/Public/static/bootstrapv3/js/bootstrap.min.js"></script>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width">
    </head>
    <body>
        <div class="hidden-print toolbarpr">             
            <button class='btn btn-primary' id='printview'>开始打印</button> 提示：Google Chrome 打印预览页边距设置为：最小值，或者自定义上下左右都为3mm。

        </div>
        <script>
            $('#printview').click(function() {
                window.print();
            })
        </script>

    <?php if(is_array($data)): $i = 0; $__LIST__ = $data;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$vodata): $mod = ($i % 2 );++$i;?><div class="printpage" style='min-height: 27.5cm; max-height: 27.5cm; margin: 0px;'>
            <div class="row">
                <div class="col-xs-5"><img width="100" src="/admexp/Public/Admin/images/print_logp.png"> Order NO.：<?php echo ($vodata["orderdata"]["oid"]); ?>  </div>
                <div class="col-xs-6 text-right"><?php echo (get_expline_name($vodata["orderdata"]["eid"])); ?> <?php echo NOW();?></div>             
            </div>

            <div>
                <table width="100%" border="0" class="table2 ">
                    <tr>
                        <th rowspan="5"  bgcolor="#F5F5F5" scope="row" width="20%" style=" text-align:center;"><img src="/admexp/Public/static/phpqrcode/QRcode.php?id=<?php echo ($vodata["orderdata"]["oid"]); ?>&type=order&size=4" ></th>
                        <th colspan="4">SHIPPER</th>
                    </tr>
                    <tr>
                        <th width="20%">NAME OF SENDER</th>
                        <td width="21%"><?php echo ($vodata["warehousedata"]["person"]); ?></td>
                        <th width="20%">TELEPHONE NO.<br/><small>(VERY IMPORTANT)</small></th>
                        <td width="27%"><?php echo ($vodata["warehousedata"]["phone"]); ?></td>
                    </tr>
                    <tr>
                        <th colspan="4">COMPANY NAME AND ADDRESS(Inculde Postal Code)</th>
                    </tr>
                    <tr>
                        <td colspan="4">                                  
                            <?php echo ($vodata["warehousedata"]["location"]); ?><br/>
                            <?php echo ($vodata["warehousedata"]["city"]); ?>, <?php echo ($vodata["warehousedata"]["province"]); ?>,<?php echo ($vodata["warehousedata"]["country"]); ?>,<?php echo ($vodata["warehousedata"]["zipcode"]); ?><br/>
                        </td>
                    </tr>
                    <tr>
                        <th>POSTAL CODE</th>
                        <td><?php echo ($vodata["warehousedata"]["zipcode"]); ?></td>
                        <th>COUNTRY</th>
                        <td><?php echo ($vodata["warehousedata"]["country"]); ?></td>
                    </tr>
                </table> 
                <table width="100%" border="0" class="table2">
                    <tr>
                        <th width="5%" rowspan="4" scope="row">RECEIVER</th>
                        <th width="20%">NAME OF CONTACT PERSON</th>
                        <td width="21%"><?php echo ($vodata["orderdata"]["name"]); ?></td>
                        <th width="27%">TELEPHONE NO.<br/><small>(VERY IMPORTANT)</samll></th>
                        <td width="27%"><?php echo ($vodata["orderdata"]["phone"]); ?></td>
                    </tr>
                    <tr>
                        <th colspan="4">COMPANY NAME AND ADDRESS(Inculde Postal/Zip Code)</th>
                    </tr>
                    <tr>
                        <td colspan="4">
                    <?php if(!empty($orderdata["street"])): echo ($vodata["orderdata"]["street"]); ?><br/><?php endif; ?>
                    <?php echo ($vodata["orderdata"]["address"]); ?> ,
                    <?php echo ($vodata["orderdata"]["city"]); ?>, <?php echo ($vodata["orderdata"]["province"]); ?>, <?php echo ($vodata["orderdata"]["country"]); ?>,<?php echo ($vodata["orderdata"]["zipcode"]); ?><br/>
                    </td>
                    </tr>
                    <tr>
                        <th>POSTAL CODE</th>
                        <td><?php echo ($vodata["orderdata"]["zipcode"]); ?></td>
                        <th>COUNTRY</th>
                        <td><?php echo ($vodata["orderdata"]["country"]); ?></td>
                    </tr>
                </table>
            </div>
            <br/>
            <hr/>
            <div class="row">
                <div class="col-xs-6"><img  width="100"  src="/admexp/Public/Admin/images/print_logp.png">出库单/<?php echo (get_expline_name($vodata["orderdata"]["eid"])); ?> /<?php echo (get_warehouse_name($vodata["orderdata"]["wid"])); ?></div>
                <div class="col-xs-4 text-right">Order NO.：<?php echo ($vodata["orderdata"]["oid"]); ?></div>             
            </div>
            <br/>
            <div class="row">
                <div class="col-xs-6">
                    <table width="100%" border="0" class="table table-condensed ">
                        <tr>
                            <th scope="col">需要出库的</th>
                            <th scope="col">包裹编码/位置/包裹描述</th>
                        </tr>


                        <?php if(is_array($vodata["packagelist"])): $i = 0; $__LIST__ = $vodata["packagelist"];if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$vo): $mod = ($i % 2 );++$i;?><tr>   
                                <td><img   src="/admexp/Public/static/phpqrcode/QRcode.php?id=<?php echo ($vo["id"]); ?>&type=package&size=3"> </td>
                                <td> 

                                    <?php echo ($vo["id"]); ?>/<?php echo ($vo["location"]); ?>/<?php echo ($vo["name"]); ?>/<?php echo ($vo["remark"]); ?> 

                                </td>  
                            </tr><?php endforeach; endif; else: echo "" ;endif; ?>



                    </table>

                </div>
                <div class="col-xs-5 ">
                    <table width="100%" border="0" class="table table-condensed table-bordered" align="center">
                        <tr>
                            <th scope="col">需要提供的服务</th>
                            <th scope="col">人工操作</th>
                        </tr>
                        <?php if(is_array($vodata["serverlist"])): $i = 0; $__LIST__ = $vodata["serverlist"];if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$vos): $mod = ($i % 2 );++$i;?><tr>
                                <td><?php echo ($vos["sname"]); ?></td>
                                <td>□</td>
                            </tr><?php endforeach; endif; else: echo "" ;endif; ?>
                    </table>
                    <table width="100%" border="0" class="table table-condensed table-bordered" align="center">
                        <tr>
                            <th scope="col">订单留言</th>

                        </tr>
                        <tr>
                            <td><?php echo ($vodata["orderdata"]["remark"]); ?></td>
                        </tr>
                    </table>
                    <table width="100%" border="0" class="table table-condensed table-bordered" align="center">
                        <tr>
                            <th scope="col">打包记录</th>
                            <th scope="col">人工操作</th>
                        </tr>

                        <tr>
                            <td>秤重</td>
                            <td></td>
                        </tr>
                        <tr>
                            <td>体积重</td>
                            <td></td>
                        </tr>
                        <tr>
                            <td>存放位置</td>
                            <td></td>
                        </tr>
                        <tr>
                            <td>员工签字</td>
                            <td></td>
                        </tr>

                    </table>
                </div>             
            </div>
        </div><?php endforeach; endif; else: echo "" ;endif; ?>
    
        
</body>
</html>