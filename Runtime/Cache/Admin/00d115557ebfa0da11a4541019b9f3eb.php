<?php if (!defined('THINK_PATH')) exit();?><!doctype html>
<html>
    <head>
        <meta charset="UTF-8">
        <title><?php echo ($meta_title); ?>|ADMEXPRESS管理平台</title>
        <link href="/admexp/Public/favicon.ico" type="image/x-icon" rel="shortcut icon">
       
        <link rel="stylesheet" type="text/css" href="/admexp/Public/Admin/css/base.css" media="all">
        <link rel="stylesheet" type="text/css" href="/admexp/Public/Admin/css/common.css" media="all">
        <link rel="stylesheet" type="text/css" href="/admexp/Public/Admin/css/module.css">
        <link rel="stylesheet" type="text/css" href="/admexp/Public/Admin/css/style.css" media="all">
        <link rel="stylesheet" type="text/css" href="/admexp/Public/Admin/css/<?php echo (C("COLOR_STYLE")); ?>.css" media="all">     
        <!--
            <link rel="stylesheet" type="text/css" href="/admexp/Public/static/font-awesome/css/font-awesome.min.css" media="all">
        -->
        <!--[if lt IE 9]>
       <script type="text/javascript" src="/admexp/Public/static/jquery-1.10.2.min.js"></script>
       <![endif]--><!--[if gte IE 9]><!-->
        <script type="text/javascript" src="/admexp/Public/static/jquery-2.0.3.min.js"></script>

        <script type="text/javascript" src="/admexp/Public/Admin/js/jquery.mousewheel.js"></script>
        <!--<![endif]-->
    
</head>
<body>
    <!-- 头部 -->
    <div class="header">
        <!-- Logo -->
        <span class="logo"></span>
        <!-- /Logo -->

        <!-- 主导航 -->
        <ul class="main-nav">
            <?php if(is_array($__MENU__["main"])): $i = 0; $__LIST__ = $__MENU__["main"];if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$menu): $mod = ($i % 2 );++$i;?><li class="<?php echo ((isset($menu["class"]) && ($menu["class"] !== ""))?($menu["class"]):''); ?>"><a href="<?php echo (u($menu["url"])); ?>"><?php echo ($menu["title"]); ?></a></li><?php endforeach; endif; else: echo "" ;endif; ?>
        </ul>
        <!-- /主导航 -->

        <!-- 用户栏 -->
        <div class="user-bar">
            <a href="javascript:;" class="user-entrance"><i class="icon-user"></i></a>
            <ul class="nav-list user-menu hidden">
                <li class="manager">你好，<em title="<?php echo session('user_auth.username');?>"><?php echo session('user_auth.username');?></em></li>
                <li><a href="<?php echo U('User/updatePassword');?>">修改密码</a></li>
                <li><a href="<?php echo U('User/updateNickname');?>">修改昵称</a></li>
                <li><a href="<?php echo U('Public/logout');?>">退出</a></li>
            </ul>
        </div>
    </div>
    <!-- /头部 -->

    <!-- 边栏 -->
    <div class="sidebar">
        <!-- 子导航 -->
        
            <div id="subnav" class="subnav">
                <?php if(!empty($_extra_menu)): ?>
                    <?php echo extra_menu($_extra_menu,$__MENU__); endif; ?>
                <?php if(is_array($__MENU__["child"])): $i = 0; $__LIST__ = $__MENU__["child"];if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$sub_menu): $mod = ($i % 2 );++$i;?><!-- 子导航 -->
                    <?php if(!empty($sub_menu)): if(!empty($key)): ?><h3><i class="icon icon-unfold"></i><?php echo ($key); ?></h3><?php endif; ?>
                        <ul class="side-sub-menu">
                            <?php if(is_array($sub_menu)): $i = 0; $__LIST__ = $sub_menu;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$menu): $mod = ($i % 2 );++$i;?><li>
                                    <a class="item" href="<?php echo (u($menu["url"])); ?>"><?php echo ($menu["title"]); ?></a>
                                </li><?php endforeach; endif; else: echo "" ;endif; ?>
                        </ul><?php endif; ?>
                    <!-- /子导航 --><?php endforeach; endif; else: echo "" ;endif; ?>
            </div>
        
        <!-- /子导航 -->
    </div>
    <!-- /边栏 -->

    <!-- 内容区 -->
    <div id="main-content">
        <div id="top-alert" class="fixed alert alert-error" style="display: none;">
            <button class="close fixed" style="margin-top: 4px;">&times;</button>
            <div class="alert-content">这是内容</div>
        </div>
        <div id="main" class="main">
            
                <!-- nav -->
                <?php if(!empty($_show_nav)): ?><div class="breadcrumb">
                        <span>您的位置:</span>
                        <?php $i = '1'; ?>
                        <?php if(is_array($_nav)): foreach($_nav as $k=>$v): if($i == count($_nav)): ?><span><?php echo ($v); ?></span>
                                <?php else: ?>
                                <span><a href="<?php echo ($k); ?>"><?php echo ($v); ?></a>&gt;</span><?php endif; ?>
                            <?php $i = $i+1; endforeach; endif; ?>
                    </div><?php endif; ?>
                <!-- nav -->
            

                
    <table width="100%">
        <tr>
            <td width="50%" valign="top" > 
                <!------左边S------>
                <div class="data-table table-striped">     
                    <table class="">
                        <thead>
                            <tr>               
                                <th   width="100">订单<?php echo ($order["id"]); ?>详情</th>
                                <th class="" width="130"></th>
                                <th class="" width="130"></th>
                                <th class=""></th>                   
                            </tr>
                        </thead>
                        <tbody>

                            <tr>
                                <th>订单状态</th>
                                <td colspan="2"><?php echo ($order["status"]); ?> | <?php echo ($order["status_text"]); ?></td>
                             
                                <td>
                                    <?php if(($order["status"]) == "3000"): ?><a href="<?php echo U('Order/packaction&oid='.$order['id']);?>" class="confirm  btn submit-btn">打包出库操作</a><?php endif; ?>
                                    <?php if(($order["status"]) == "5000"): ?><a href="<?php echo U('Order/outputaction?oid='.$order['id']);?>" class="confirm  btn submit-btn">确认发货操作</a><?php endif; ?>
                                    <a href="<?php echo U('Order/packprint?oid='.$order['id']);?>" class=" btn" target="_blank">出库单打印</a>
                                </td>
                            </tr>
                            <tr>
                                <th>订单包裹重量</th>
                        <form action="<?php echo U('weightmodfiyaction');?>" method="post" class="form-horizonta0"> 
                            <input type="hidden" name="oid" value="<?php echo ($order["id"]); ?>"/> 
                            <td> 称重<input type="text" name="weight" class="text input-2x myinput" value="<?php echo ($order["weight"]); ?>" /> g</td>
                            <td> 体积重<input type="text" name="bulkfactor" class="text input-2x myinput" value="<?php echo ($order["bulkfactor"]); ?>" /> g</td>
                            <td> <?php if(($order["status"]) == "4000"): ?><button class="btn submit-btn ajax-post  confirm" id="submit" type="submit" target-form="form-horizonta0">修改包裹重量</button><?php endif; ?></td>
                        </form>
                        </tr>
                        <tr>
                            <th>订单金额</th>
                        <form action="<?php echo U('paymodfiyaction');?>" method="post" class="form-horizontal">
                            <input type="hidden" name="oid" value="<?php echo ($order["id"]); ?>"/> 
                            <td> <input type="text" name="money" class="text input-2x myinput" value="<?php echo ($order["money"]); ?>" /> 元</td>
                            <td> <input type="text" name="remark" class="text input-2x myinput" value="修改理由" /></td>
                            <td><?php if(($order["status"]) == "4000"): ?><button class="btn submit-btn  ajax-post  confirm" id="submit" type="submit" target-form="form-horizontal">修改订单金额</button><?php endif; ?></td>
                        </form>
                        </tr>
                        <tr>
                            <th>物流公司</th>                                
                        <form action="<?php echo U('outputmodfiyaction');?>" method="post" class="form-horizontal2">
                            <input type="hidden" name="oid" value="<?php echo ($order["id"]); ?>"/> 
                            <td>  <input type="text" name="expname" class="text input-2x myinput" value="<?php echo ($order["expname"]); ?>" /></td>
                            <td>  <input type="text" name="expnumber" class="text input-2x myinput" value="<?php echo ($order["expnumber"]); ?>" /></td>
                            <td> <?php if(($order["status"]) == "6000"): ?><button class="btn submit-btn ajax-post confirm" id="submit" type="submit" target-form="form-horizontal2">修改物流信息</button><?php endif; ?></td>
                        </form>
                        </tr>
                        <tr>
                            <th>订单用户备注</th>
                            <td> <?php echo ($order["remark"]); ?></td>
                            <td> </td>
                            <td> </td>
                        </tr>
                        </tbody>

                    </table>
                </div>
                <!------左边E------>
            </td>
            <td valign="top" style=" background-color: #FFFAEA"> 
                <!------右边S------>
                <div class="data-table table-striped" style=" border-left: 3px solid black">     
                    <table class="">
                        <thead>
                            <tr>               



                                <th class="" width="130">处理时间</th>
                                <th class="" >处理信息</th>
                                <th class="">操作人</th>                   
                            </tr>
                        </thead>
                        <tbody>
                        <?php if(is_array($logs)): $i = 0; $__LIST__ = $logs;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$vo): $mod = ($i % 2 );++$i;?><tr>

                                <td><?php echo (time_format($vo["createdate"])); ?></td>
                                <td> <?php echo ($vo["remark"]); ?></td>
                                <td> <?php echo ($vo["username"]); ?></td>
                            </tr><?php endforeach; endif; else: echo "" ;endif; ?>

                        </tbody>
                    </table>
                </div>
                <!------右边E------>
            </td>
        </tr>
    </table>


    <style>

        .img2wei{margin: 2px;}
        .table2{border: 1px solid #000;}
        .table2 tr th{
            background-color: #F5F5F5;
            font-size: 9px;
            font-weight: normal;
            border: 1px solid #000;
            padding-left: 5px;
        }
        .table2 tr td{
            border: 1px solid #000;
            padding-left: 5px;
            font-weight: bold;
        }   
        .toolbarpr{
            padding:20px;
            border-bottom: 2px solid #006dcc;
            background-color: #091620;
            color: #fff;
        }

    </style>



    <?php if(is_array($data)): $i = 0; $__LIST__ = $data;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$vodata): $mod = ($i % 2 );++$i;?><div class="printpage" style='min-height: 27.5cm; max-height: 27.5cm; margin: 0px;'>
            <div class="row" style=" background: #000;color: #fff; height: 30px;line-height: 30px;text-align: center;">
                <div class="col-xs-5"> Order NO.：<?php echo ($vodata["orderdata"]["oid"]); ?>  </div>          
            </div>

            <div >
                <table width="100%" border="0" class="table2 ">
                    <tr>
                        <th rowspan="5"  bgcolor="#F5F5F5" scope="row" width="20%" style=" text-align:center;"><img src="/admexp/Public/static/phpqrcode/QRcode.php?id=<?php echo ($vodata["orderdata"]["oid"]); ?>&type=order&size=4" ></th>
                        <th colspan="4">SHIPPER</th>
                    </tr>
                    <tr>
                        <th width="20%">NAME OF SENDER</th>
                        <td width="21%"><?php echo ($vodata["warehousedata"]["person"]); ?></td>
                        <th width="20%">TELEPHONE NO.<br/><small>(VERY IMPORTANT)</small></th>
                        <td width="27%"><?php echo ($vodata["warehousedata"]["phone"]); ?></td>
                    </tr>
                    <tr>
                        <th colspan="4">COMPANY NAME AND ADDRESS(Inculde Postal Code)</th>
                    </tr>
                    <tr>
                        <td colspan="4">                                  
                            <?php echo ($vodata["warehousedata"]["location"]); ?><br/>
                            <?php echo ($vodata["warehousedata"]["city"]); ?>, <?php echo ($vodata["warehousedata"]["province"]); ?>,<?php echo ($vodata["warehousedata"]["country"]); ?>,<?php echo ($vodata["warehousedata"]["zipcode"]); ?><br/>
                        </td>
                    </tr>
                    <tr>
                        <th>POSTAL CODE</th>
                        <td><?php echo ($vodata["warehousedata"]["zipcode"]); ?></td>
                        <th>COUNTRY</th>
                        <td><?php echo ($vodata["warehousedata"]["country"]); ?></td>
                    </tr>
                </table> 
                <table width="100%" border="0" class="table2">
                    <tr>
                        <th width="5%" rowspan="4" scope="row">RECEIVER</th>
                        <th width="20%">NAME OF CONTACT PERSON</th>
                        <td width="21%"><?php echo ($vodata["orderdata"]["name"]); ?></td>
                        <th width="27%">TELEPHONE NO.<br/><small>(VERY IMPORTANT)</samll></th>
                        <td width="27%"><?php echo ($vodata["orderdata"]["phone"]); ?></td>
                    </tr>
                    <tr>
                        <th colspan="4">COMPANY NAME AND ADDRESS(Inculde Postal/Zip Code)</th>
                    </tr>
                    <tr>
                        <td colspan="4">
                    <?php if(!empty($orderdata["street"])): echo ($vodata["orderdata"]["street"]); ?><br/><?php endif; ?>
                    <?php echo ($vodata["orderdata"]["address"]); ?> ,
                    <?php echo ($vodata["orderdata"]["city"]); ?>, <?php echo ($vodata["orderdata"]["province"]); ?>, <?php echo ($vodata["orderdata"]["country"]); ?>,<?php echo ($vodata["orderdata"]["zipcode"]); ?><br/>
                    </td>
                    </tr>
                    <tr>
                        <th>POSTAL CODE</th>
                        <td><?php echo ($vodata["orderdata"]["zipcode"]); ?></td>
                        <th>COUNTRY</th>
                        <td><?php echo ($vodata["orderdata"]["country"]); ?></td>
                    </tr>
                </table>
            </div>
            <br/>
            <hr/>
            <div class="row">
                <div class="col-xs-6">出库单/<?php echo (get_expline_name($vodata["orderdata"]["eid"])); ?> /<?php echo (get_warehouse_name($vodata["orderdata"]["wid"])); ?></div>

            </div>
            <br/>
            <div class="row">
                <div class="col-xs-6">
                    <table width="100%" border="0" class="table2 table-condensed ">
                        <tr >
                            <th scope="col"  width="20%">需要出库的</th>
                            <th scope="col">包裹编码/位置/包裹描述</th>
                        </tr>

                        <?php if(is_array($vodata["packagelist"])): $i = 0; $__LIST__ = $vodata["packagelist"];if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$vo): $mod = ($i % 2 );++$i;?><tr>   
                                <td   style=" text-align:center;"> <img   src="/admexp/Public/static/phpqrcode/QRcode.php?id=<?php echo ($vo["id"]); ?>&type=package&size=3"> </td>
                                <td> 

                                    <?php echo ($vo["id"]); ?>/<?php echo ($vo["location"]); ?>/<?php echo ($vo["name"]); ?>/<?php echo ($vo["remark"]); ?> <BR/>
                                    <span style="color: red;border-bottom: 1px solid #000"><?php if(($vo["bulkfactor"]) > $vo["weight"]): echo ($vo["bulkfactor"]); else: echo ($vo["weight"]); endif; ?>g</span>

                                </td>  
                            </tr><?php endforeach; endif; else: echo "" ;endif; ?>

                    </table>

                </div>

            </div>
        </div><?php endforeach; endif; else: echo "" ;endif; ?>



        </div>
        <div class="cont-ft">
            <div class="copyright">
                <div class="fl">感谢使用<a href="http://www.admexpress.cn" target="_blank">ADMEXPRESS</a>管理平台</div>
                <div class="fr">V<?php echo (ADMEXPRESS_VERSION); ?></div>
            </div>
        </div>
    </div>
    <!-- /内容区 -->
    <script type="text/javascript">
        (function() {
            var ThinkPHP = window.Think = {
                "ROOT": "/admexp", //当前网站地址
                "APP": "/admexp/index.php", //当前项目地址
                "PUBLIC": "/admexp/Public", //项目公共目录地址
                "DEEP": "<?php echo C('URL_PATHINFO_DEPR');?>", //PATHINFO分割符
                "MODEL": ["<?php echo C('URL_MODEL');?>", "<?php echo C('URL_CASE_INSENSITIVE');?>", "<?php echo C('URL_HTML_SUFFIX');?>"],
                "VAR": ["<?php echo C('VAR_MODULE');?>", "<?php echo C('VAR_CONTROLLER');?>", "<?php echo C('VAR_ACTION');?>"]
            }
        })();
    </script>
    <script type="text/javascript" src="/admexp/Public/static/think.js"></script>
    <script type="text/javascript" src="/admexp/Public/Admin/js/common.js"></script>
    <script type="text/javascript">
        +function() {
            var $window = $(window), $subnav = $("#subnav"), url;
            $window.resize(function() {
                $("#main").css("min-height", $window.height() - 130);
            }).resize();

            /* 左边菜单高亮 */
            url = window.location.pathname + window.location.search;
            url = url.replace(/(\/(p)\/\d+)|(&p=\d+)|(\/(id)\/\d+)|(&id=\d+)|(\/(group)\/\d+)|(&group=\d+)/, "");
            $subnav.find("a[href='" + url + "']").parent().addClass("current");

            /* 左边菜单显示收起 */
            $("#subnav").on("click", "h3", function() {
                var $this = $(this);
                $this.find(".icon").toggleClass("icon-fold");
                $this.next().slideToggle("fast").siblings(".side-sub-menu:visible").
                        prev("h3").find("i").addClass("icon-fold").end().end().hide();
            });

            $("#subnav h3 a").click(function(e) {
                e.stopPropagation()
            });

            /* 头部管理员菜单 */
            $(".user-bar").mouseenter(function() {
                var userMenu = $(this).children(".user-menu ");
                userMenu.removeClass("hidden");
                clearTimeout(userMenu.data("timeout"));
            }).mouseleave(function() {
                var userMenu = $(this).children(".user-menu");
                userMenu.data("timeout") && clearTimeout(userMenu.data("timeout"));
                userMenu.data("timeout", setTimeout(function() {
                    userMenu.addClass("hidden")
                }, 100));
            });

            /* 表单获取焦点变色 */
            $("form").on("focus", "input", function() {
                $(this).addClass('focus');
            }).on("blur", "input", function() {
                $(this).removeClass('focus');
            });
            $("form").on("focus", "textarea", function() {
                $(this).closest('label').addClass('focus');
            }).on("blur", "textarea", function() {
                $(this).closest('label').removeClass('focus');
            });

            // 导航栏超出窗口高度后的模拟滚动条
            var sHeight = $(".sidebar").height();
            var subHeight = $(".subnav").height();
            var diff = subHeight - sHeight; //250
            var sub = $(".subnav");
            if (diff > 0) {
                $(window).mousewheel(function(event, delta) {
                    if (delta > 0) {
                        if (parseInt(sub.css('marginTop')) > -10) {
                            sub.css('marginTop', '0px');
                        } else {
                            sub.css('marginTop', '+=' + 10);
                        }
                    } else {
                        if (parseInt(sub.css('marginTop')) < '-' + (diff - 10)) {
                            sub.css('marginTop', '-' + (diff - 10));
                        } else {
                            sub.css('marginTop', '-=' + 10);
                        }
                    }
                });
            }
        }();
    </script>

    <script src="/admexp/Public/static/thinkbox/jquery.thinkbox.js"></script>

    <script type="text/javascript">

//导航高亮
        highlight_subnav('<?php echo U('Order/index');?>');
    </script>

</body>
</html>