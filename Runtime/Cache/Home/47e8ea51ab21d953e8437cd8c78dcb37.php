<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE HTML>
<html>
<head>
	<meta charset="UTF-8">
<title><?php echo C('WEB_SITE_TITLE');?></title>
<link href="/admexp/Public/static/bootstrapv3/css/bootstrap.css" rel="stylesheet">
<link href="/admexp/Public/static/bootstrapv3/css/bootstrap-theme.css" rel="stylesheet">
<link href="/admexp/Public/Home/css/style.css" rel="stylesheet">
<link href="/admexp/Public/Home/css/home.css" rel="stylesheet">

<!--
<link href="/admexp/Public/static/bootstrap/css/docs.css" rel="stylesheet">
<link href="/admexp/Public/static/bootstrap/css/onethink.css" rel="stylesheet">-->

<!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
<!--[if lt IE 9]>
<script src="/admexp/Public/static/bootstrap/js/html5shiv.js"></script>
<![endif]-->

<!--[if lt IE 9]>
<script type="text/javascript" src="/admexp/Public/static/jquery-1.10.2.min.js"></script>
<![endif]-->
<!--[if gte IE 9]><!-->
<script type="text/javascript" src="/admexp/Public/static/jquery-2.0.3.min.js"></script>
<script type="text/javascript" src="/admexp/Public/static/bootstrapv3/js/bootstrap.min.js"></script>
<!--<![endif]-->
<!-- 页面header钩子，一般用于加载插件CSS文件和代码 -->
<?php echo hook('pageHeader');?>

</head>
<body>
	<!-- 头部 -->
	<!-- 导航条
================================================== -->

<nav class="navbar navbar-default" role="navigation" style="margin-bottom: 0px;">

    <div class="container" style='padding: 30px 0 30px 0;'>
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">            
            <a class="navbar-brand" href="#" style="margin-top: -15px;"><img alt="Brand" width="250" height="50" src="/admexp/Public/Home/images/hlogo.png"/></a>
        </div>

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav">

                <?php $__NAV__ = M('Channel')->field(true)->where("status=1")->order("sort")->select(); if(is_array($__NAV__)): $i = 0; $__LIST__ = $__NAV__;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$nav): $mod = ($i % 2 );++$i; if(($nav["pid"]) == "0"): ?><li>
                        <a href="<?php echo (get_nav_url($nav["url"])); ?>" target="<?php if(($nav["target"]) == "1"): ?>_blank<?php else: ?>_self<?php endif; ?>"><?php echo ($nav["title"]); ?></a>
                    </li><?php endif; endforeach; endif; else: echo "" ;endif; ?>              
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">常见问题 <span class="caret"></span></a>
                    <ul class="dropdown-menu" role="menu">
                      <!--  <li><a href="#">联系客服</a></li>
                        <li class="divider"></li>-->
                          <?php echo W('Category/lists', array(1, true));?> 
                    </ul>
                </li>
            </ul>

            <ul class="nav navbar-nav navbar-right">
                <?php if(is_login()): ?><li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown"> <span class="glyphicon glyphicon-th-list"></span> 控制中心 <span class="caret"></span></a>
                        <ul class="dropdown-menu" role="menu">
                            <li><a href="<?php echo U('Center/index');?>">预报管理</a></li>                           
                            <li><a href="<?php echo U('Center/warehouse','CID=-1');?>">仓库管理</a></li>
                            <li><a href="<?php echo U('Center/order');?>">订单管理</a></li>
                        </ul>
                    </li>
                    
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown"> <span class="glyphicon glyphicon-user"></span> <?php echo get_username();?>  <span class="caret"></span></a>
                        <ul class="dropdown-menu" role="menu">
                             <li><a href="<?php echo U('Center/accountdetail');?>">财务明细</a></li>
                             <li><a href="<?php echo U('Center/myaddress');?>">国内地址</a></li>
                            <li><a href="<?php echo U('User/profile');?>">修改密码</a></li>                          
                            <li class="divider"></li>
                            <li><a href="<?php echo U('User/logout');?>">退出</a></li>
                        </ul>
                    </li>
                    <?php else: ?>
                    <li><a href="<?php echo U('User/login');?>">登录</a></li>
                    <li><a href="<?php echo U('User/register');?>" style="padding-left:0;padding-right:0">注册</a></li><?php endif; ?>
            </ul>
        </div><!-- /.navbar-collapse -->
    </div><!-- /.container-fluid -->

</nav>

	<!-- /头部 -->
	
	<!-- 主体 -->
	
    <header class="jumbotron subhead" id="overview" style='background-color: #2C3E50;color: #fff;text-align: center;'>
        <div class="container" >
            <div class="col-sm-3">
                帐号余额 
                <div style="font-size: 30pt;padding:10px;"><?php echo get_member_balance(session('user_auth.uid'));?></div>         

            </div>
            <div class="col-sm-3">
                仓库货物
                <div style="font-size: 30pt;padding:10px;"><?php echo get_warehousepackage_count(session('user_auth.uid'));?></div>         

            </div>
            <div class="col-sm-3">
                转运途中
                <div style="font-size: 30pt;padding:10px;"><?php echo get_orderstatus_count(session('user_auth.uid'),6000);?></div>        
            </div>
            <div class="col-sm-3">
                安全到达
                <div style="font-size: 30pt;padding:10px;"><?php echo get_orderstatus_count(session('user_auth.uid'),7000);?></div>        
            </div>
            <!-------快捷导航----->
            <div style="position:  absolute;top: 264px;" >
                <ul class="nav nav-tabs" id='headerNav' role="tablist">
                    <li role="presentation" <?php if(($nvazhi) == "1"): ?>class="active"<?php endif; ?>><a href="<?php echo U('Center/index');?>">预报</a></li>
                    <li role="presentation" <?php if(($nvazhi) == "2"): ?>class="active"<?php endif; ?>><a href="<?php echo U('Center/warehouse','CID=-1');?>">仓库</a></li>
                    <li role="presentation" <?php if(($nvazhi) == "3"): ?>class="active"<?php endif; ?>><a href="<?php echo U('Center/order');?>">订单</a></li>
                  <li role="presentation" <?php if(($nvazhi) == "4"): ?>class="active"<?php endif; ?>><a href="<?php echo U('Center/accountdetail');?>">财务</a></li>
                </ul>
            </div>
        </div>

    </header>





<div id="main-container" class="container">
    
    <!----第一次使用还没有包裹---->
    <div class="row" style="margin-bottom: 30px;"  <?php if(empty($_list)): ?>style="display:none;"<?php endif; ?>>
        <div class="col-md-10">
            <ul class="nav nav-pills" role="tablist">
                <li role="presentation" <?php if(($type) == "-1"): ?>class="active"<?php endif; ?>><a href="<?php echo U('order','type=-1');?>">全部</a></li>
                <li role="presentation" <?php if(($type) == "3000"): ?>class="active"<?php endif; ?>><a href="<?php echo U('order','type=3000');?>">打包称重</a></li>
                <li role="presentation" <?php if(($type) == "4000"): ?>class="active"<?php endif; ?>><a href="<?php echo U('order','type=4000');?>">等待付款</a></li>
                <li role="presentation" <?php if(($type) == "5000"): ?>class="active"<?php endif; ?>><a href="<?php echo U('order','type=5000');?>">等待发货</a></li>
                <li role="presentation" <?php if(($type) == "6000"): ?>class="active"<?php endif; ?>><a href="<?php echo U('order','type=6000');?>">运输途中</a></li>
                <li role="presentation" <?php if(($type) == "7000"): ?>class="active"<?php endif; ?>><a href="<?php echo U('order','type=7000');?>">签收完毕</a></li>
            </ul>
        </div>
        <div class="col-md-2"><a class="btn btn-success" href="<?php echo U('Center/warehouse','CID=-1');?>">+ 申请出库转运</a></div>
    </div>
    <hr/>
    <div style="padding: 5px;background-color: #D7F3EE">
        <table class="table table-striped" style="background-color: #fff">
            <thead>
                <tr>

                    <th class="">订单编号</th>
                    <th class="">仓库名称</th>       
                    <th class="">包裹物品</th>    
                    <th class="">包裹重量</th>
                    <th class="">运单金额</th>
                    <th class="">状态</th>              
                    <th class="">操作</th>
                </tr>
            </thead>
            <tbody>
            <?php if(!empty($_list)): if(is_array($_list)): $i = 0; $__LIST__ = $_list;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$vo): $mod = ($i % 2 );++$i;?><tr>
                        <td><?php echo ($vo["id"]); ?> </td>	
                        <td><?php echo (get_warehouse_name($vo["wid"])); ?> </td>
                        <td>
                    <?php if(is_array($vo["packages"])): $i = 0; $__LIST__ = $vo["packages"];if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$vs): $mod = ($i % 2 );++$i;?>[<?php echo ($vs["id"]); ?>]<b><?php echo ($vs["name"]); ?></b>                       
                        <br/><?php endforeach; endif; else: echo "" ;endif; ?>
                    </td>
                    <td><?php if(($vo["bulkfactor"]) > $vo["weight"]): ?>体积重：<?php echo (get_kg($vo["bulkfactor"])); ?>KG<?php else: ?>秤重：<?php echo (get_kg($vo["weight"])); ?>KG<?php endif; ?></td>
                    <td class="text-center"><?php if(($vo["money"]) > "0"): ?><span style="font-weight: bold;color:#17AE92"><?php echo ($vo["money"]); ?>元</span><?php else: ?>尚未计算<?php endif; ?></td>
                    <td style=" border-right: #F3F3F3 solid 3px;  border-left: #F3F3F3 solid 3px; text-align:left"><?php echo ($vo["status"]); ?>|<?php echo ($vo["status_text"]); ?><i class='glyphicon glyphicon-info-sign' style='color: #D5D5D5;padding-left: 3px;'></i>
                        <div class="progress">
                            <div class="progress-bar progress-bar-success progress-bar-striped active"  role="progressbar" aria-valuenow="<?php echo ($vo["status"]); ?>" aria-valuemin="0" aria-valuemax="7000" style="width: <?php echo (get_baifengbi($vo["status"])); ?>%">
                                <span class="sr-only">45% Complete</span>
                            </div>
                        </div>

                    <td style="text-align: left;">
                        <a href="<?php echo U('Center/details','oid='.$vo['id'].'');?>" class="btn-default btn">订单详情</a>
                        <?php if(in_array(($vo["status"]), explode(',',"3000,4000"))): ?><a href="<?php echo U('Center/pay?oid='.$vo['id']);?>" class='btn-danger btn  <?php if(($vo["status"]) == "3000"): ?>disabled<?php endif; ?> <qt name="vo.status" value="4000">hidden</qt>'>我要付款</a><?php endif; ?>
                        <?php if(in_array(($vo["status"]), explode(',',"5000,6000"))): ?><a href="<?php echo U('Center/signin?oid='.$vo['id']);?>" class='btn-success btn  <?php if(($vo["status"]) == "5000"): ?>disabled<?php endif; ?> <qt name="vo.status" value="6000">hidden</qt>'>确认收货</a><?php endif; ?>
                    </td>
                    </tr><?php endforeach; endif; else: echo "" ;endif; ?>
                <?php else: ?>
                <td colspan="10" class="text-center" style="padding: 20px;"> 暂时还没有数据! </td><?php endif; ?>

        </table>
    </div>



</div>

<script type="text/javascript">
    $(function() {
        $(window).resize(function() {
            $("#main-container").css("min-height", $(window).height() - 383);
        }).resize();
    })
</script>
	<!-- /主体 -->

	<!-- 底部 -->
	
    <!-- 底部
    ================================================== -->
    
<div class="footer">
  <div class="container">
    <p class="text-muted"> <p> 本程序 <strong><a href="http://www.onethink.cn" target="_blank">AdmExpress</a></strong> 强力驱动 <?php echo C('WEB_SITE_ICP');?></p></p>
  </div>
</div>

<script type="text/javascript">
(function(){
	var ThinkPHP = window.Think = {
		"ROOT"   : "/admexp", //当前网站地址
		"APP"    : "/admexp/index.php", //当前项目地址
		"PUBLIC" : "/admexp/Public", //项目公共目录地址
		"DEEP"   : "<?php echo C('URL_PATHINFO_DEPR');?>", //PATHINFO分割符
		"MODEL"  : ["<?php echo C('URL_MODEL');?>", "<?php echo C('URL_CASE_INSENSITIVE');?>", "<?php echo C('URL_HTML_SUFFIX');?>"],
		"VAR"    : ["<?php echo C('VAR_MODULE');?>", "<?php echo C('VAR_CONTROLLER');?>", "<?php echo C('VAR_ACTION');?>"]
	}
})();
</script>
 <!-- 用于加载js代码 -->
<!-- 页面footer钩子，一般用于加载插件JS文件和JS代码 -->
<?php echo hook('pageFooter', 'widget');?>
<div class="hidden"><!-- 用于加载统计代码等隐藏元素 -->
	
</div>

	<!-- /底部 -->
</body>
</html>