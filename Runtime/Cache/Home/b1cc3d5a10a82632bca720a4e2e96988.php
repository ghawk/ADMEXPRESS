<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE HTML>
<html>
<head>
	<meta charset="UTF-8">
<title><?php echo C('WEB_SITE_TITLE');?></title>
<link href="/admexp/Public/static/bootstrapv3/css/bootstrap.css" rel="stylesheet">
<link href="/admexp/Public/static/bootstrapv3/css/bootstrap-theme.css" rel="stylesheet">
<link href="/admexp/Public/Home/css/style.css" rel="stylesheet">
<link href="/admexp/Public/Home/css/home.css" rel="stylesheet">

<!--
<link href="/admexp/Public/static/bootstrap/css/docs.css" rel="stylesheet">
<link href="/admexp/Public/static/bootstrap/css/onethink.css" rel="stylesheet">-->

<!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
<!--[if lt IE 9]>
<script src="/admexp/Public/static/bootstrap/js/html5shiv.js"></script>
<![endif]-->

<!--[if lt IE 9]>
<script type="text/javascript" src="/admexp/Public/static/jquery-1.10.2.min.js"></script>
<![endif]-->
<!--[if gte IE 9]><!-->
<script type="text/javascript" src="/admexp/Public/static/jquery-2.0.3.min.js"></script>
<script type="text/javascript" src="/admexp/Public/static/bootstrapv3/js/bootstrap.min.js"></script>
<!--<![endif]-->
<!-- 页面header钩子，一般用于加载插件CSS文件和代码 -->
<?php echo hook('pageHeader');?>

</head>
<body>
	<!-- 头部 -->
	<!-- 导航条
================================================== -->

<nav class="navbar navbar-default" role="navigation" style="margin-bottom: 0px;">

    <div class="container" style='padding: 30px 0 30px 0;'>
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">            
            <a class="navbar-brand" href="#" style="margin-top: -15px;"><img alt="Brand" width="250" height="50" src="/admexp/Public/Home/images/hlogo.png"/></a>
        </div>

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav">

                <?php $__NAV__ = M('Channel')->field(true)->where("status=1")->order("sort")->select(); if(is_array($__NAV__)): $i = 0; $__LIST__ = $__NAV__;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$nav): $mod = ($i % 2 );++$i; if(($nav["pid"]) == "0"): ?><li>
                        <a href="<?php echo (get_nav_url($nav["url"])); ?>" target="<?php if(($nav["target"]) == "1"): ?>_blank<?php else: ?>_self<?php endif; ?>"><?php echo ($nav["title"]); ?></a>
                    </li><?php endif; endforeach; endif; else: echo "" ;endif; ?>              
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">常见问题 <span class="caret"></span></a>
                    <ul class="dropdown-menu" role="menu">
                      <!--  <li><a href="#">联系客服</a></li>
                        <li class="divider"></li>-->
                          <?php echo W('Category/lists', array(1, true));?> 
                    </ul>
                </li>
            </ul>

            <ul class="nav navbar-nav navbar-right">
                <?php if(is_login()): ?><li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown"> <span class="glyphicon glyphicon-th-list"></span> 控制中心 <span class="caret"></span></a>
                        <ul class="dropdown-menu" role="menu">
                            <li><a href="<?php echo U('Center/index');?>">预报管理</a></li>                           
                            <li><a href="<?php echo U('Center/warehouse');?>&CID=-1">仓库管理</a></li>
                            <li><a href="<?php echo U('Center/order');?>">订单管理</a></li>
                        </ul>
                    </li>
                    
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown"> <span class="glyphicon glyphicon-user"></span> <?php echo get_username();?>  <span class="caret"></span></a>
                        <ul class="dropdown-menu" role="menu">
                             <li><a href="<?php echo U('Center/accountdetail');?>">财务明细</a></li>
                             <li><a href="<?php echo U('Center/myaddress');?>">国内地址</a></li>
                            <li><a href="<?php echo U('User/profile');?>">修改密码</a></li>                          
                            <li class="divider"></li>
                            <li><a href="<?php echo U('User/logout');?>">退出</a></li>
                        </ul>
                    </li>
                    <?php else: ?>
                    <li><a href="<?php echo U('User/login');?>">登录</a></li>
                    <li><a href="<?php echo U('User/register');?>" style="padding-left:0;padding-right:0">注册</a></li><?php endif; ?>
            </ul>
        </div><!-- /.navbar-collapse -->
    </div><!-- /.container-fluid -->

</nav>

	<!-- /头部 -->
	
	<!-- 主体 -->
	
    <header class="jumbotron subhead" id="overview" style='background-color: #2C3E50;color: #fff;text-align: center;'>
        <div class="container" >
            <div class="col-sm-3">
                帐号余额 
                <div style="font-size: 30pt;padding:10px;"><?php echo get_member_balance(session('user_auth.uid'));?></div>         

            </div>
            <div class="col-sm-3">
                仓库货物
                <div style="font-size: 30pt;padding:10px;"><?php echo get_warehousepackage_count(session('user_auth.uid'));?></div>         

            </div>
            <div class="col-sm-3">
                转运途中
                <div style="font-size: 30pt;padding:10px;"><?php echo get_orderstatus_count(session('user_auth.uid'),6000);?></div>        
            </div>
            <div class="col-sm-3">
                安全到达
                <div style="font-size: 30pt;padding:10px;"><?php echo get_orderstatus_count(session('user_auth.uid'),7000);?></div>        
            </div>
            <!-------快捷导航----->
            <div style="position:  absolute;top: 264px;" >
                <ul class="nav nav-tabs" id='headerNav' role="tablist">
                    <li role="presentation" <?php if(($nvazhi) == "1"): ?>class="active"<?php endif; ?>><a href="<?php echo U('Center/index');?>">预报</a></li>
                    <li role="presentation" <?php if(($nvazhi) == "2"): ?>class="active"<?php endif; ?>><a href="<?php echo U('Center/warehouse','CID=-1');?>">仓库</a></li>
                    <li role="presentation" <?php if(($nvazhi) == "3"): ?>class="active"<?php endif; ?>><a href="<?php echo U('Center/order');?>">订单</a></li>
                  <li role="presentation" <?php if(($nvazhi) == "4"): ?>class="active"<?php endif; ?>><a href="<?php echo U('Center/accountdetail');?>">财务</a></li>
                </ul>
            </div>
        </div>

    </header>





<div id="main-container" class="container">
    
    <div class="panel panel-default">
        <!-- Default panel contents -->
        <div class="panel-heading">收货地址</div>
        <div class="panel-body">
            <p>新增加收货地址</p>
            <form class="form-horizontal" id="addressform" role="form"  action="<?php echo U('myaddressadd');?>" method="POST">
                <div class="form-group">
                    <label class="col-sm-2 control-label">国家选择</label>
                    <div class="col-sm-10">
                        <select name="countryid" class="form-control" >
                            <?php if(is_array($countrylist)): foreach($countrylist as $key=>$vo): ?><option value="<?php echo ($vo["country_id"]); ?>">[<?php echo ($vo["code"]); ?>] <?php echo ($vo["name_chinese"]); ?></option><?php endforeach; endif; ?>     

                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label">国家</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" name="country" placeholder="国家名称" datatype="*" nullmsg="请填写国家名称！">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label">省/州</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" name="province" placeholder="省/州名称" datatype="*" nullmsg="请填写省/州名称！">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label">城市</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" name="city" placeholder="城市名称" datatype="*" nullmsg="请填写城市名称！">
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-2 control-label">详细的地址</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" name="location" placeholder="详细的街道地址" datatype="*" nullmsg="请填写详细的地址！">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label">邮政编码</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" name="zipcode" placeholder="邮政编码" datatype="*" nullmsg="请填写邮政编码！">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label">收货人姓名</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" name="name" placeholder="收货人姓名" datatype="*" nullmsg="请填收货人姓名！">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label">收货人电话</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" name="phone" placeholder="收货人电话" datatype="*" nullmsg="请填收货人电话！">
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-offset-2 col-sm-10">
                        <button type="submit" class="btn btn-success"> <span class="glyphicon glyphicon-book"></span> 添加收货地址  </button>
                    </div>
                </div>
            </form>
            <!------------------->
        </div>
        <script>
            $("#addressform").Validform();
        </script>
        <!-- Table -->
        <table class="table table-hover">
            <thead>
                <tr>
                    <th>收货人</th>
                    <th>国家</th>
                    <th>所在地区</th>
                    <th>街道地址</th>
                    <th>邮编</th>
                    <th>联系电话</th>
                    <th>操作</th>
                </tr>
            </thead>
            <tbody>
                <?php if(is_array($addresslist)): $i = 0; $__LIST__ = $addresslist;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$vo): $mod = ($i % 2 );++$i;?><tr>
                    <td><?php echo ($vo["name"]); ?></td>
                    <td><?php echo ($vo["country"]); ?></td>
                    <td><?php echo ($vo["province"]); echo ($vo["city"]); ?></td>
                    <td><?php echo ($vo["location"]); ?></td>
                    <td><?php echo ($vo["zipcode"]); ?></td>
                    <td><?php echo ($vo["phone"]); ?></td>
                    <td><a href="<?php echo U('myaddressedit?id='.$vo['id'].'');?>">编辑</a>   <a href="<?php echo U('myaddressdel?id='.$vo['id'].'');?>" onclick="javascript:return de_confirm();">删除</a></td>
                </tr><?php endforeach; endif; else: echo "" ;endif; ?>
            </tbody>
        </table>
    </div>

</div>

<script type="text/javascript">
    $(function() {
        $(window).resize(function() {
            $("#main-container").css("min-height", $(window).height() - 383);
        }).resize();
    })
</script>
	<!-- /主体 -->

	<!-- 底部 -->
	
    <!-- 底部
    ================================================== -->
    
<div class="footer">
  <div class="container">
    <p class="text-muted"> <p> 本程序 <strong><a href="http://www.onethink.cn" target="_blank">AdmExpress</a></strong> 强力驱动 <?php echo C('WEB_SITE_ICP');?></p></p>
  </div>
</div>

<script type="text/javascript">
(function(){
	var ThinkPHP = window.Think = {
		"ROOT"   : "/admexp", //当前网站地址
		"APP"    : "/admexp/index.php", //当前项目地址
		"PUBLIC" : "/admexp/Public", //项目公共目录地址
		"DEEP"   : "<?php echo C('URL_PATHINFO_DEPR');?>", //PATHINFO分割符
		"MODEL"  : ["<?php echo C('URL_MODEL');?>", "<?php echo C('URL_CASE_INSENSITIVE');?>", "<?php echo C('URL_HTML_SUFFIX');?>"],
		"VAR"    : ["<?php echo C('VAR_MODULE');?>", "<?php echo C('VAR_CONTROLLER');?>", "<?php echo C('VAR_ACTION');?>"]
	}
})();
</script>
 <!-- 用于加载js代码 -->
<!-- 页面footer钩子，一般用于加载插件JS文件和JS代码 -->
<?php echo hook('pageFooter', 'widget');?>
<div class="hidden"><!-- 用于加载统计代码等隐藏元素 -->
	
</div>

	<!-- /底部 -->
</body>
</html>