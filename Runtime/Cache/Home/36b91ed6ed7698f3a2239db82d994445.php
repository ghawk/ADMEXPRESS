<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE HTML>
<html>
<head>
	<meta charset="UTF-8">
<title><?php echo C('WEB_SITE_TITLE');?></title>
<link href="/admexp/Public/static/bootstrapv3/css/bootstrap.css" rel="stylesheet">
<link href="/admexp/Public/static/bootstrapv3/css/bootstrap-theme.css" rel="stylesheet">
<link href="/admexp/Public/Home/css/style.css" rel="stylesheet">
<link href="/admexp/Public/Home/css/home.css" rel="stylesheet">

<!--
<link href="/admexp/Public/static/bootstrap/css/docs.css" rel="stylesheet">
<link href="/admexp/Public/static/bootstrap/css/onethink.css" rel="stylesheet">-->

<!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
<!--[if lt IE 9]>
<script src="/admexp/Public/static/bootstrap/js/html5shiv.js"></script>
<![endif]-->

<!--[if lt IE 9]>
<script type="text/javascript" src="/admexp/Public/static/jquery-1.10.2.min.js"></script>
<![endif]-->
<!--[if gte IE 9]><!-->
<script type="text/javascript" src="/admexp/Public/static/jquery-2.0.3.min.js"></script>
<script type="text/javascript" src="/admexp/Public/static/bootstrapv3/js/bootstrap.min.js"></script>
<!--<![endif]-->
<!-- 页面header钩子，一般用于加载插件CSS文件和代码 -->
<?php echo hook('pageHeader');?>

</head>
<body>
	<!-- 头部 -->
	<!-- 导航条
================================================== -->

<nav class="navbar navbar-default" role="navigation" style="margin-bottom: 0px;">

    <div class="container" style='padding: 30px 0 30px 0;'>
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">            
            <a class="navbar-brand" href="#" style="margin-top: -15px;"><img alt="Brand" width="250" height="50" src="/admexp/Public/Home/images/hlogo.png"/></a>
        </div>

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav">

                <?php $__NAV__ = M('Channel')->field(true)->where("status=1")->order("sort")->select(); if(is_array($__NAV__)): $i = 0; $__LIST__ = $__NAV__;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$nav): $mod = ($i % 2 );++$i; if(($nav["pid"]) == "0"): ?><li>
                        <a href="<?php echo (get_nav_url($nav["url"])); ?>" target="<?php if(($nav["target"]) == "1"): ?>_blank<?php else: ?>_self<?php endif; ?>"><?php echo ($nav["title"]); ?></a>
                    </li><?php endif; endforeach; endif; else: echo "" ;endif; ?>              
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">常见问题 <span class="caret"></span></a>
                    <ul class="dropdown-menu" role="menu">
                      <!--  <li><a href="#">联系客服</a></li>
                        <li class="divider"></li>-->
                          <?php echo W('Category/lists', array(1, true));?> 
                    </ul>
                </li>
            </ul>

            <ul class="nav navbar-nav navbar-right">
                <?php if(is_login()): ?><li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown"> <span class="glyphicon glyphicon-th-list"></span> 控制中心 <span class="caret"></span></a>
                        <ul class="dropdown-menu" role="menu">
                            <li><a href="<?php echo U('Center/index');?>">预报管理</a></li>                           
                            <li><a href="<?php echo U('Center/warehouse');?>&CID=-1">仓库管理</a></li>
                            <li><a href="<?php echo U('Center/order');?>">订单管理</a></li>
                        </ul>
                    </li>
                    
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown"> <span class="glyphicon glyphicon-user"></span> <?php echo get_username();?>  <span class="caret"></span></a>
                        <ul class="dropdown-menu" role="menu">
                             <li><a href="<?php echo U('Center/accountdetail');?>">财务明细</a></li>
                             <li><a href="<?php echo U('Center/myaddress');?>">国内地址</a></li>
                            <li><a href="<?php echo U('User/profile');?>">修改密码</a></li>                          
                            <li class="divider"></li>
                            <li><a href="<?php echo U('User/logout');?>">退出</a></li>
                        </ul>
                    </li>
                    <?php else: ?>
                    <li><a href="<?php echo U('User/login');?>">登录</a></li>
                    <li><a href="<?php echo U('User/register');?>" style="padding-left:0;padding-right:0">注册</a></li><?php endif; ?>
            </ul>
        </div><!-- /.navbar-collapse -->
    </div><!-- /.container-fluid -->

</nav>

	<!-- /头部 -->
	
	<!-- 主体 -->
	
    <header class="jumbotron subhead" id="overview" style='background-color: #2C3E50;color: #fff;text-align: center;'>
        <div class="container" >
            <div class="col-sm-3">
                帐号余额 
                <div style="font-size: 30pt;padding:10px;"><?php echo get_member_balance(session('user_auth.uid'));?></div>         

            </div>
            <div class="col-sm-3">
                仓库货物
                <div style="font-size: 30pt;padding:10px;"><?php echo get_warehousepackage_count(session('user_auth.uid'));?></div>         

            </div>
            <div class="col-sm-3">
                转运途中
                <div style="font-size: 30pt;padding:10px;"><?php echo get_orderstatus_count(session('user_auth.uid'),6000);?></div>        
            </div>
            <div class="col-sm-3">
                安全到达
                <div style="font-size: 30pt;padding:10px;"><?php echo get_orderstatus_count(session('user_auth.uid'),7000);?></div>        
            </div>
            <!-------快捷导航----->
            <div style="position:  absolute;top: 264px;" >
                <ul class="nav nav-tabs" id='headerNav' role="tablist">
                    <li role="presentation" <?php if(($nvazhi) == "1"): ?>class="active"<?php endif; ?>><a href="<?php echo U('Center/index');?>">预报</a></li>
                    <li role="presentation" <?php if(($nvazhi) == "2"): ?>class="active"<?php endif; ?>><a href="<?php echo U('Center/warehouse','CID=-1');?>">仓库</a></li>
                    <li role="presentation" <?php if(($nvazhi) == "3"): ?>class="active"<?php endif; ?>><a href="<?php echo U('Center/order');?>">订单</a></li>
                  <li role="presentation" <?php if(($nvazhi) == "4"): ?>class="active"<?php endif; ?>><a href="<?php echo U('Center/accountdetail');?>">财务</a></li>
                </ul>
            </div>
        </div>

    </header>





<div id="main-container" class="container">
    


    <div class="panel panel-success">
        <div class="panel-heading">
            <div class="row">
                <div class="col-md-10"> <h4>订单号：<?php echo ($data["id"]); ?></h4> </div>
                <div class="col-md-2 text-right">
                    <a href="javascript:history.go(-1);" class='btn btn-success'>返回</a>
                 <?php if(in_array(($data["status"]), explode(',',"3000,4000"))): ?><a href="<?php echo U('Center/pay?oid='.$data['id']);?>" class='btn-danger btn  <?php if(($data["status"]) == "3000"): ?>disabled<?php endif; ?> <qt name="data.status" value="4000">hidden</qt>'>我要付款</a><?php endif; ?>
                    <?php if(in_array(($data["status"]), explode(',',"5000,6000"))): ?><a href="<?php echo U('Center/signin?oid='.$data['id']);?>" class='btn-success btn  <?php if(($data["status"]) == "5000"): ?>disabled<?php endif; ?> <qt name="data.status" value="6000">hidden</qt>'>确认收货</a><?php endif; ?>
                </div>
            </div>

        </div>
        <div class="panel-body">
            <?php echo ($data["status_text"]["remark"]); ?>
        </div>
    </div>




    <div class="progress">
        <div class="progress-bar progress-bar-success progress-bar-striped active"  role="progressbar" aria-valuenow="<?php echo ($vo["status"]); ?>" aria-valuemin="0" aria-valuemax="7000" style="width: <?php echo (get_baifengbi($data["status"])); ?>%">
            <span class="sr-only">45% Complete</span>
        </div>
    </div>



    <div class="panel" style="padding: 5px; background-color: #DCEED4">
        <div style="background-color: #fff">
            <!-- Nav tabs -->
            <ul class="nav nav-pills" role="tablist" style="padding: 5px;">
                <li role="presentation" class="active"><a href="#home" role="tab" data-toggle="tab">订单跟踪</a></li>
                <li role="presentation"><a href="#profile" role="tab" data-toggle="tab">付款信息</a></li>

            </ul>

            <!-- Tab panes -->
            <div class="tab-content">
                <div role="tabpanel" class="tab-pane active" id="home" style="padding: 5px;">
                    <table class="table">
                        <tr>
                            <th>处理时间</th>
                            <th>处理信息</th>
                            <th>操作人</th>
                        </tr>
                        <?php if(is_array($data["logs"])): $i = 0; $__LIST__ = $data["logs"];if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$vo): $mod = ($i % 2 );++$i;?><tr>
                                <td><?php echo (time_format($vo["createdate"])); ?></td>
                                <td> <?php echo ($vo["remark"]); ?></td>
                                <td> <?php echo ($vo["username"]); ?></td>
                            </tr><?php endforeach; endif; else: echo "" ;endif; ?>
                    </table>                 
                </div>
                <div role="tabpanel" class="tab-pane" id="profile"></div>  
            </div>
        </div>
    </div>

    <div class="panel panel-default">
        <div class="panel-heading">
            <div class="row">
                <div class="col-md-10"> <h4>订单详情</h4> </div>
                <div class="col-md-2 text-right"></div>
            </div>

        </div>
        <div class="panel-body">

            <address>
                <strong>收货人信息</strong><br>
                收 货 人：<?php echo ($data["name"]); ?><br>
                地    址：<?php echo ($data["country"]); ?>,<?php echo ($data["province"]); ?>,<?php echo ($data["city"]); ?>,<?php echo ($data["address"]); ?><br>
                手机号码：<?php echo ($data["phone"]); ?>
            </address>
            <hr>
            <address>
                <strong>物流信息</strong><br>
                物流线路：<?php echo ($data["eline"]["name"]); ?>（首重<?php echo ($data["eline"]["fweight"]); ?>g <?php echo ($data["eline"]["fprice"]); ?>元 / 续重<?php echo ($data["eline"]["cweight"]); ?>g <?php echo ($data["eline"]["cprice"]); ?>元）<br>
                物流公司：<?php echo ($data["expname"]); ?><br>
                跟踪号码：<?php echo ($data["expnumber"]); ?>
            </address>
            <hr>
            <address>
                <strong>商品信息</strong><br>
                <div class="row" >
                    <div class="col-md-12" style="padding: 5px;">
                        <table width="100%" border="0" class="table table-condensed ">
                            <tr >
                                <th scope="col"  width="20%">需要出库的</th>
                                <th scope="col">包裹编码 </th>
                                <th>包裹描述</th>
                                <th>包裹重量</th>

                            </tr>

                            <?php if(is_array($data["packages"])): $i = 0; $__LIST__ = $data["packages"];if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$vo): $mod = ($i % 2 );++$i;?><tr>   
                                    <td   style=" text-align:center;"> <img   src="/admexp/Public/static/phpqrcode/QRcode.php?id=<?php echo ($vo["id"]); ?>&type=package&size=3"> </td>
                                    <td> 
                                        <?php echo ($vo["id"]); ?>   
                                    </td>  
                                    <td><?php echo ($vo["name"]); ?></td>
                                    <td>
                                        <span style="color: red;border-bottom: 1px solid #000"><?php if(($vo["bulkfactor"]) > $vo["weight"]): echo ($vo["bulkfactor"]); else: echo ($vo["weight"]); endif; ?>g</span>
                                    </td>

                                </tr><?php endforeach; endif; else: echo "" ;endif; ?>

                        </table>

                    </div>

                </div>
            </address>
            <hr>
            <div class='row'>
                <div class='col-md-8'><span class="badge">励志</span>业精于勤而荒于嬉，行成于思而毁于随。</div>
                <div class='col-md-4 text-right' style="font-size: 15pt;">订单金额：<span style="font-weight: bold;color:red"><?php echo ($data["money"]); ?>元</span></div>
            </div>
      
        </div>
    </div>


</div>

<script type="text/javascript">
    $(function() {
        $(window).resize(function() {
            $("#main-container").css("min-height", $(window).height() - 383);
        }).resize();
    })
</script>
	<!-- /主体 -->

	<!-- 底部 -->
	
    <!-- 底部
    ================================================== -->
    
<div class="footer">
  <div class="container">
    <p class="text-muted"> <p> 本程序 <strong><a href="http://www.onethink.cn" target="_blank">AdmExpress</a></strong> 强力驱动 <?php echo C('WEB_SITE_ICP');?></p></p>
  </div>
</div>

<script type="text/javascript">
(function(){
	var ThinkPHP = window.Think = {
		"ROOT"   : "/admexp", //当前网站地址
		"APP"    : "/admexp/index.php", //当前项目地址
		"PUBLIC" : "/admexp/Public", //项目公共目录地址
		"DEEP"   : "<?php echo C('URL_PATHINFO_DEPR');?>", //PATHINFO分割符
		"MODEL"  : ["<?php echo C('URL_MODEL');?>", "<?php echo C('URL_CASE_INSENSITIVE');?>", "<?php echo C('URL_HTML_SUFFIX');?>"],
		"VAR"    : ["<?php echo C('VAR_MODULE');?>", "<?php echo C('VAR_CONTROLLER');?>", "<?php echo C('VAR_ACTION');?>"]
	}
})();
</script>
 <!-- 用于加载js代码 -->
<!-- 页面footer钩子，一般用于加载插件JS文件和JS代码 -->
<?php echo hook('pageFooter', 'widget');?>
<div class="hidden"><!-- 用于加载统计代码等隐藏元素 -->
	
</div>

	<!-- /底部 -->
</body>
</html>