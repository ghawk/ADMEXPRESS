<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE HTML>
<html>
<head>
	<meta charset="UTF-8">
<title><?php echo C('WEB_SITE_TITLE');?></title>
<link href="/admexp/Public/static/bootstrapv3/css/bootstrap.css" rel="stylesheet">
<link href="/admexp/Public/static/bootstrapv3/css/bootstrap-theme.css" rel="stylesheet">
<link href="/admexp/Public/Home/css/style.css" rel="stylesheet">
<link href="/admexp/Public/Home/css/home.css" rel="stylesheet">

<!--
<link href="/admexp/Public/static/bootstrap/css/docs.css" rel="stylesheet">
<link href="/admexp/Public/static/bootstrap/css/onethink.css" rel="stylesheet">-->

<!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
<!--[if lt IE 9]>
<script src="/admexp/Public/static/bootstrap/js/html5shiv.js"></script>
<![endif]-->

<!--[if lt IE 9]>
<script type="text/javascript" src="/admexp/Public/static/jquery-1.10.2.min.js"></script>
<![endif]-->
<!--[if gte IE 9]><!-->
<script type="text/javascript" src="/admexp/Public/static/jquery-2.0.3.min.js"></script>
<script type="text/javascript" src="/admexp/Public/static/bootstrapv3/js/bootstrap.min.js"></script>
<!--<![endif]-->
<!-- 页面header钩子，一般用于加载插件CSS文件和代码 -->
<?php echo hook('pageHeader');?>

</head>
<body>
	<!-- 头部 -->
	<!-- 导航条
================================================== -->

<nav class="navbar navbar-default" role="navigation" style="margin-bottom: 0px;">

    <div class="container" style='padding: 30px 0 30px 0;'>
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">            
            <a class="navbar-brand" href="#" style="margin-top: -15px;"><img alt="Brand" width="250" height="50" src="/admexp/Public/Home/images/hlogo.png"/></a>
        </div>

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav">

                <?php $__NAV__ = M('Channel')->field(true)->where("status=1")->order("sort")->select(); if(is_array($__NAV__)): $i = 0; $__LIST__ = $__NAV__;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$nav): $mod = ($i % 2 );++$i; if(($nav["pid"]) == "0"): ?><li>
                        <a href="<?php echo (get_nav_url($nav["url"])); ?>" target="<?php if(($nav["target"]) == "1"): ?>_blank<?php else: ?>_self<?php endif; ?>"><?php echo ($nav["title"]); ?></a>
                    </li><?php endif; endforeach; endif; else: echo "" ;endif; ?>              
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">常见问题 <span class="caret"></span></a>
                    <ul class="dropdown-menu" role="menu">
                      <!--  <li><a href="#">联系客服</a></li>
                        <li class="divider"></li>-->
                          <?php echo W('Category/lists', array(1, true));?> 
                    </ul>
                </li>
            </ul>

            <ul class="nav navbar-nav navbar-right">
                <?php if(is_login()): ?><li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown"> <span class="glyphicon glyphicon-th-list"></span> 控制中心 <span class="caret"></span></a>
                        <ul class="dropdown-menu" role="menu">
                            <li><a href="<?php echo U('Center/index');?>">预报管理</a></li>                           
                            <li><a href="<?php echo U('Center/warehouse');?>&CID=-1">仓库管理</a></li>
                            <li><a href="<?php echo U('Center/order');?>">订单管理</a></li>
                        </ul>
                    </li>
                    
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown"> <span class="glyphicon glyphicon-user"></span> <?php echo get_username();?>  <span class="caret"></span></a>
                        <ul class="dropdown-menu" role="menu">
                             <li><a href="<?php echo U('Center/accountdetail');?>">财务明细</a></li>
                             <li><a href="<?php echo U('Center/myaddress');?>">国内地址</a></li>
                            <li><a href="<?php echo U('User/profile');?>">修改密码</a></li>                          
                            <li class="divider"></li>
                            <li><a href="<?php echo U('User/logout');?>">退出</a></li>
                        </ul>
                    </li>
                    <?php else: ?>
                    <li><a href="<?php echo U('User/login');?>">登录</a></li>
                    <li><a href="<?php echo U('User/register');?>" style="padding-left:0;padding-right:0">注册</a></li><?php endif; ?>
            </ul>
        </div><!-- /.navbar-collapse -->
    </div><!-- /.container-fluid -->

</nav>

	<!-- /头部 -->
	
	<!-- 主体 -->
	
    <header class="jumbotron subhead" id="overview" style='background-color: #2C3E50;color: #fff;text-align: center;'>
        <div class="container" >
            <div class="col-sm-3">
                帐号余额 
                <div style="font-size: 30pt;padding:10px;"><?php echo get_member_balance(session('user_auth.uid'));?></div>         

            </div>
            <div class="col-sm-3">
                仓库货物
                <div style="font-size: 30pt;padding:10px;"><?php echo get_warehousepackage_count(session('user_auth.uid'));?></div>         

            </div>
            <div class="col-sm-3">
                转运途中
                <div style="font-size: 30pt;padding:10px;"><?php echo get_orderstatus_count(session('user_auth.uid'),6000);?></div>        
            </div>
            <div class="col-sm-3">
                安全到达
                <div style="font-size: 30pt;padding:10px;"><?php echo get_orderstatus_count(session('user_auth.uid'),7000);?></div>        
            </div>
            <!-------快捷导航----->
            <div style="position:  absolute;top: 264px;" >
                <ul class="nav nav-tabs" id='headerNav' role="tablist">
                    <li role="presentation" <?php if(($nvazhi) == "1"): ?>class="active"<?php endif; ?>><a href="<?php echo U('Center/index');?>">预报</a></li>
                    <li role="presentation" <?php if(($nvazhi) == "2"): ?>class="active"<?php endif; ?>><a href="<?php echo U('Center/warehouse','CID=-1');?>">仓库</a></li>
                    <li role="presentation" <?php if(($nvazhi) == "3"): ?>class="active"<?php endif; ?>><a href="<?php echo U('Center/order');?>">订单</a></li>
                  <li role="presentation" <?php if(($nvazhi) == "4"): ?>class="active"<?php endif; ?>><a href="<?php echo U('Center/accountdetail');?>">财务</a></li>
                </ul>
            </div>
        </div>

    </header>





<div id="main-container" class="container">
    
    <div class="row " style="">
        <div class="col-md-6">
            <div class="page-header">
                <h1><?php echo ($vo["name"]); ?> <small>为您服务！</small></h1>
            </div>
            <div>
                <address>
                    收件人(person)：<strong><?php echo ($vo["person"]); ?></strong><br>
                    国家(Country)：	<?php echo ($vo["country"]); ?> <br> 
                    州/省(Province)：	<?php echo ($vo["province"]); ?> <br> 
                    城市(City)：	<?php echo ($vo["city"]); ?>	<br> 
                    地址1(Address)：		<?php echo ($vo["location"]); ?> #<strong class="text-danger">U<?php echo session('user_auth.uid');?></strong><br>                    
                    邮编(Zip Code)：	<?php echo ($vo["zipcode"]); ?><br>
                    电话(Tel)：<?php echo ($vo["phone"]); ?>
                </address>
                <small style="color: #17AE92;"><?php echo ($vo["remark"]); ?></small> 
            </div>
        </div>
        <div class="col-md-6">
            <div class="panel panel-default">
                <div class="panel-heading">包裹入库预报单</div>
                <div class="panel-body">
                    <form role="form" action="<?php echo U('Center/Packageadd');?>" method="POST" id="addfrom" class="demoform">             
                        <input type="hidden" name="TOKEN" value="<?php echo session('TOKEN');?>">
                        <input type="hidden" name="warehouse_id" value="<?php echo ($vo["id"]); ?>">
                        <input type="hidden" name="warehouse_name" value="<?php echo ($vo["name"]); ?>">    
                        <div class="form-group">
                            <label>包裹名称</label>
                            <input type="text" class="form-control" id="forecast_exp_name" name="package_name" placeholder="请简单输入包裹的名称以便于记忆" datatype="*" nullmsg="请填写包裹的名称！">
                        </div>
                        <div class="form-group">
                            <label>快递公司名称</label>
                            <input type="text" class="form-control" id="forecast_exp_name" name="forecast_exp_name" placeholder="输入快递公司名称比如 UPS/Fedex" datatype="*" nullmsg="请输入快递公司名称比如 UPS/Fedex！">
                        </div>
                        <div class="form-group">
                            <label>快递公司单号</label>
                            <input type="text" class="form-control" id="forecast_exp_number" name="forecast_exp_number" placeholder="输入快递公司运单号比如 1215121215" datatype="*" nullmsg="请输入快递公司运单号比如 1215121215!">
                        </div>
                        <div class="form-group">
                            <label>包裹申报价值</label>
                            <div class="input-group">
                                <input type="text" class="form-control" placeholder="输入包裹的申报价值" name="package_price" datatype="*" nullmsg="请输入包裹的申报价值！">
                                <span class="input-group-addon">元</span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label>包裹内物品描述</label>
                            <textarea class="form-control" id="package_caption" name="package_caption" placeholder="请描述包裹内物品以及附加消息" rows="5" datatype="*" nullmsg="请请描述包裹内物品以及附加消息！"></textarea>
                        </div>
                        <div class="text-center">
                            <hr class="featurette-divider">                                                              
                            <button type="submit" id="subbtn" class="btn btn-success">提交入库预报单</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>


</div>

<script type="text/javascript">
    $(function() {
        $(window).resize(function() {
            $("#main-container").css("min-height", $(window).height() - 383);
        }).resize();
    })
</script>
	<!-- /主体 -->

	<!-- 底部 -->
	
    <!-- 底部
    ================================================== -->
    
<div class="footer">
  <div class="container">
    <p class="text-muted"> <p> 本程序 <strong><a href="http://www.onethink.cn" target="_blank">AdmExpress</a></strong> 强力驱动 <?php echo C('WEB_SITE_ICP');?></p></p>
  </div>
</div>

<script type="text/javascript">
(function(){
	var ThinkPHP = window.Think = {
		"ROOT"   : "/admexp", //当前网站地址
		"APP"    : "/admexp/index.php", //当前项目地址
		"PUBLIC" : "/admexp/Public", //项目公共目录地址
		"DEEP"   : "<?php echo C('URL_PATHINFO_DEPR');?>", //PATHINFO分割符
		"MODEL"  : ["<?php echo C('URL_MODEL');?>", "<?php echo C('URL_CASE_INSENSITIVE');?>", "<?php echo C('URL_HTML_SUFFIX');?>"],
		"VAR"    : ["<?php echo C('VAR_MODULE');?>", "<?php echo C('VAR_CONTROLLER');?>", "<?php echo C('VAR_ACTION');?>"]
	}
})();
</script>
 <!-- 用于加载js代码 -->
<!-- 页面footer钩子，一般用于加载插件JS文件和JS代码 -->
<?php echo hook('pageFooter', 'widget');?>
<div class="hidden"><!-- 用于加载统计代码等隐藏元素 -->
	
</div>

	<!-- /底部 -->
</body>
</html>