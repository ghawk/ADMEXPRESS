<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE HTML>
<html>
<head>
	<meta charset="UTF-8">
<title><?php echo C('WEB_SITE_TITLE');?></title>
<link href="/admexp/Public/static/bootstrapv3/css/bootstrap.css" rel="stylesheet">
<link href="/admexp/Public/static/bootstrapv3/css/bootstrap-theme.css" rel="stylesheet">
<link href="/admexp/Public/Home/css/style.css" rel="stylesheet">
<link href="/admexp/Public/Home/css/home.css" rel="stylesheet">

<!--
<link href="/admexp/Public/static/bootstrap/css/docs.css" rel="stylesheet">
<link href="/admexp/Public/static/bootstrap/css/onethink.css" rel="stylesheet">-->

<!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
<!--[if lt IE 9]>
<script src="/admexp/Public/static/bootstrap/js/html5shiv.js"></script>
<![endif]-->

<!--[if lt IE 9]>
<script type="text/javascript" src="/admexp/Public/static/jquery-1.10.2.min.js"></script>
<![endif]-->
<!--[if gte IE 9]><!-->
<script type="text/javascript" src="/admexp/Public/static/jquery-2.0.3.min.js"></script>
<script type="text/javascript" src="/admexp/Public/static/bootstrapv3/js/bootstrap.min.js"></script>
<!--<![endif]-->
<!-- 页面header钩子，一般用于加载插件CSS文件和代码 -->
<?php echo hook('pageHeader');?>

</head>
<body>
	<!-- 头部 -->
	<!-- 导航条
================================================== -->

<nav class="navbar navbar-default" role="navigation" style="margin-bottom: 0px;">

    <div class="container" style='padding: 30px 0 30px 0;'>
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">            
            <a class="navbar-brand" href="#" style="margin-top: -15px;"><img alt="Brand" width="250" height="50" src="/admexp/Public/Home/images/hlogo.png"/></a>
        </div>

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav">

                <?php $__NAV__ = M('Channel')->field(true)->where("status=1")->order("sort")->select(); if(is_array($__NAV__)): $i = 0; $__LIST__ = $__NAV__;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$nav): $mod = ($i % 2 );++$i; if(($nav["pid"]) == "0"): ?><li>
                        <a href="<?php echo (get_nav_url($nav["url"])); ?>" target="<?php if(($nav["target"]) == "1"): ?>_blank<?php else: ?>_self<?php endif; ?>"><?php echo ($nav["title"]); ?></a>
                    </li><?php endif; endforeach; endif; else: echo "" ;endif; ?>              
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">常见问题 <span class="caret"></span></a>
                    <ul class="dropdown-menu" role="menu">
                      <!--  <li><a href="#">联系客服</a></li>
                        <li class="divider"></li>-->
                          <?php echo W('Category/lists', array(1, true));?> 
                    </ul>
                </li>
            </ul>

            <ul class="nav navbar-nav navbar-right">
                <?php if(is_login()): ?><li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown"> <span class="glyphicon glyphicon-th-list"></span> 控制中心 <span class="caret"></span></a>
                        <ul class="dropdown-menu" role="menu">
                            <li><a href="<?php echo U('Center/index');?>">预报管理</a></li>                           
                            <li><a href="<?php echo U('Center/warehouse','CID=-1');?>">仓库管理</a></li>
                            <li><a href="<?php echo U('Center/order');?>">订单管理</a></li>
                        </ul>
                    </li>
                    
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown"> <span class="glyphicon glyphicon-user"></span> <?php echo get_username();?>  <span class="caret"></span></a>
                        <ul class="dropdown-menu" role="menu">
                             <li><a href="<?php echo U('Center/accountdetail');?>">财务明细</a></li>
                             <li><a href="<?php echo U('Center/myaddress');?>">国内地址</a></li>
                            <li><a href="<?php echo U('User/profile');?>">修改密码</a></li>                          
                            <li class="divider"></li>
                            <li><a href="<?php echo U('User/logout');?>">退出</a></li>
                        </ul>
                    </li>
                    <?php else: ?>
                    <li><a href="<?php echo U('User/login');?>">登录</a></li>
                    <li><a href="<?php echo U('User/register');?>" style="padding-left:0;padding-right:0">注册</a></li><?php endif; ?>
            </ul>
        </div><!-- /.navbar-collapse -->
    </div><!-- /.container-fluid -->

</nav>

	<!-- /头部 -->
	
	<!-- 主体 -->
	

    <div id='index-container'>
        <div class='container rightbackgroundindex'id="index-haibao">
            <div style='padding: 60px 0 0px 0;'>
                <h1><strong>做最高效的转运公司</strong></h1><br/>
                <p style='font-size:15pt; padding: 0px 100px 0 100px;'>无论您住在何处，都不要担心海外购物，您可以使用我们的转运服务。在全球各地购买商品通过我们转运至您的目的地国家！</p>
            </div>
            <div class='row' id="index-haibao-pic">
                <div class='col-sm-8'><img src="/admexp/Public/Home/images/picture.jpg" width="650"></div>
                <div class='col-sm-4 '>
                    <form role="form" action="<?php echo U('User/register');?>" target="_blank" class="hide">
                        <div class="form-group">
                            <label for="exampleInputEmail1">用户名</label>
                            <input type="email" class="form-control" id="exampleInputEmail1" placeholder="创建用户名">
                        </div>
                        <div class="form-group">
                            <label for="exampleInputEmail1">电子邮箱</label>
                            <input type="email" class="form-control" id="exampleInputEmail1" placeholder="请认真填写注册邮箱">
                        </div>
                        <div class="form-group">
                            <label for="exampleInputEmail1">密码</label>
                            <input type="email" class="form-control" id="exampleInputEmail1" placeholder="请认真填写注册密码">
                        </div>
                        <div class="form-group">                           
                            <button type="submit" class="btn btn-default" style="width: 100%;height: 50px;">开启您的专属仓库</button>
                        </div>
                    </form>

                    <div class="security_logoes hide">
                        <a class="logo_norton" href="https://trustsealinfo.verisign.com/splash?form_file=fdf/splash.fdf&amp;dn=fundbox.com&amp;lang=en" target="_blank"></a>
                        <a class="logo_bbb" target="_blank" id="bbblink" href="http://www.bbb.org/greater-san-francisco/business-reviews/financial-services/fundbox-in-san-francisco-ca-462656#bbbseal" title="Fundbox, Inc. is a BBB Accredited Financial Service in San Francisco, CA"></a>
                        <a class="logo_mcafee" href="https://www.mcafeesecure.com/verify?host=www.fundbox.com" target="_blank"></a>
                    </div>

                </div>
            </div>
        </div>
    </div>
    
        <!-------------第二层------------->
    <div style=" background-color: #EEEEEE;background-image: linear-gradient(to bottom,#fff 0,#f8f8f8 100%);">

        <div class="container">
            <div class="row" style="padding-top:20px; padding-bottom:10px;">
                <div class="col-xs-6">
                    <p class="lead text-info">关于ADMExpress</p>
                    <p>ADMEXPRESS为全球从事电子商务运输以及物流管理的企业和个人服务。拥有全球最先进的物流配送中心，网络服务，我们提供安全，快速和可靠的方式来发送和接收您的海外包裹并提供存储。我们还提供了在欧洲，亚洲和美洲的货物接收地址，通过在线管理系统我们提供了一个快速和用户友好的方式来控制您的包裹并接收您的转运订单。 <a href="#">了解更多</a></p>
                </div>  

                <div class="col-xs-3">
                    <p align="right"><span class="label label-default">运单统计</span><br><br><span class="HomeTipH3">300,020</span> <br><br>越来越多人在使用我们的系统!</p>            
                </div>  
                <div class="col-xs-3">
                    <p align="right">  <span class="glyphicon glyphicon-signal"></span> <a href="#">查看更多统计数据</a><br><br><span class="HomeTipH3"><small>337,863,600</small></span><small><br><br>已经在ADMExpress完成的转运金额</small></p><small> 
                </small></div><small>  
            </small></div><small>   
        </small></div><small>

    </small>
    </div>
 
    
    
    <!-------------第三层------------->
    
    <div class="container" style="margin-bottom: 20px; padding-bottom: 50px; padding-top: 30px;background-color: #fff;">


        <!-- START THE FEATURETTES -->
        <div class="row">
            <div class="col-lg-9" style="padding-left: 50px;">

                <h3 style="color: #31A991;">全球购物转运平台</h3>
                无论您住在何处，都不要担心海外购物，您可以使用我们的转运服务。在全球各地购买商品通过我们转运至您的目的地国家！
                <hr>
                <ul class="list-inline" id="incme">
                    <li>
                        <div class="media">
                            <a class="pull-left" href="#">
                                <i class="glyphicon glyphicon-globe ico"></i>
                            </a>
                            <div class="media-body">
                                <h4 class="media-heading">您可以全球购物</h4>
                                我们为您网上购物提供了在多个国家接收地址，您用我们的地址购物,我们将把包裹转运给您。                         
                            </div>
                        </div>
                    </li>
                    <li>
                        <div class="media">
                            <a class="pull-left" href="#">
                                <i class="glyphicon glyphicon-plane ico"></i>
                            </a>
                            <div class="media-body">

                                <h4 class="media-heading">节省运输成本</h4>
                                国际航运单包导致成本飙升，与众多物流公司合作，集中处理会员的转运包裹，降低运费80%!
                            </div>
                        </div>
                    </li>
                    <li>
                        <div class="media">
                            <a class="pull-left" href="#">
                                <i class="glyphicon glyphicon-home ico"></i>
                            </a>
                            <div class="media-body">
                                <h4 class="media-heading">全数字化智能仓库</h4>
                                我们的仓库采用物联网技术，所有的货物全部数字化管理，用户可以随时随刻管理您的包裹。
                            </div>
                        </div>
                    </li>
                    <li>
                        <div class="media">
                            <a class="pull-left" href="#">
                                <i class="glyphicon glyphicon-import ico"></i>
                            </a>
                            <div class="media-body">
                                <h4 class="media-heading">极速签收入库</h4>
                                我们采用最先进的技术，当您的货物抵达我们仓库时，货物将即刻签收并入库。
                            </div>
                        </div>
                    </li>
                    <li>
                        <div class="media">
                            <a class="pull-left" href="#">
                                <i class="glyphicon glyphicon-camera ico"></i>
                            </a>
                            <div class="media-body">
                                <h4 class="media-heading">默认入库免费拍照</h4>
                                货物入库操作时，我们的工作人员将会进行拍照并呈现给您，这一切都是免费的。
                            </div>
                        </div>
                    </li>
                    <li>
                        <div class="media">
                            <a class="pull-left" href="#">
                                <i class="glyphicon glyphicon-cutlery ico"></i>
                            </a>
                            <div class="media-body">
                                <h4 class="media-heading">丰富多样的服务</h4>
                                货物抵达我们的仓库，我们可以为您的货物提供各种各样的增值服务！
                            </div>
                        </div>
                    </li>

                    <li>
                        <div class="media">
                            <a class="pull-left" href="#">
                                <i class="glyphicon glyphicon-export ico"></i>
                            </a>
                            <div class="media-body">
                                <h4 class="media-heading">转运线路任意选</h4>
                                我们与众多海内外物流公司合作，客户可以自由选择我们提供的转运线路。
                            </div>
                        </div>
                    </li>

                    <li>
                        <div class="media">
                            <a class="pull-left" href="#">
                                <i class="glyphicon glyphicon-open ico"></i>
                            </a>
                            <div class="media-body">
                                <h4 class="media-heading">免费提供清关服务</h4>
                                当您的货物抵达目的地国家，我们将会免费为您提供清关报关服务！关税可以直接通过我们缴纳。
                            </div>
                        </div>
                    </li>

                    <li>
                        <div class="media">
                            <a class="pull-left" href="#">
                                <i class="glyphicon glyphicon-credit-card ico"></i>
                            </a>
                            <div class="media-body">
                                <h4 class="media-heading">运费资金安全保障</h4>
                                每一笔货物转运订单，您的运费将由第三方公司中介，货物安全抵达并签收，我们才能获得运费。
                            </div>
                        </div>
                    </li>

                    <li>
                        <div class="media">
                            <a class="pull-left" href="#">
                                <i class="glyphicon glyphicon-phone ico"></i>
                            </a>
                            <div class="media-body">
                                <h4 class="media-heading">支持手机端全程跟踪</h4>
                                手机客户端是专为客户量身打造的移动操作软件，为用户提供快捷方便的手机操作新体验。
                            </div>
                        </div>
                    </li>

                </ul>
            </div>
            <div class="col-lg-3">
                <div style="border-left: 1px dotted #CCC; padding-bottom: 20px; padding-left: 20px;min-height: 665px;">
                    <h3>Tracking</h3>
                    输入您的货物跟踪代码！
                    <textarea style="width: 100%;height: 100px; margin-bottom: 5px; font-size: 20px;" id="admcode" placeholder="1000000"></textarea> 
                    <div id="f1"> 
                        <input type="text" name="vcode" style="width: 80px;height:30px;" id="verifytext">  <button class="btn btn-default btn-sm" id="Trackbtn">查询</button>
                        <div id="f2"> <img src="/index.php?s=/home/util/verify.html" width="200" height="88" id="verifyimgTracking" style="margin-top: 5px; display: none;">  </div>
                    </div>
                    <hr>
                    <img width="250" class="featurette-image img-responsive" data-src="/Public/Home/js/holder.js/200x120/auto" alt="200x120" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAMgAAAB4CAYAAAC3kr3rAAAEDElEQVR4Xu3ZUU4UYRREYQgJ+yJh2ayJBZAQjSZjBjKD1TXqg/X5JHKv/depPnQD96+vr9/u/EEAgYsE7gnizkDgOgGCuDsQ+IIAQdweCBDEPYBAR8ATpONma4QAQUaKFrMjQJCOm60RAgQZKVrMjgBBOm62RggQZKRoMTsCBOm42RohQJCRosXsCBCk42ZrhABBRooWsyNAkI6brRECBBkpWsyOAEE6brZGCBBkpGgxOwIE6bjZGiFAkJGixewIEKTjZmuEAEFGihazI0CQjputEQIEGSlazI4AQTputkYIEGSkaDE7AgTpuNkaIUCQkaLF7AgQpONma4QAQUaKFrMjQJCOm60RAgQZKVrMjgBBOm62RggQZKRoMTsCBOm42RohQJCRosXsCBCk42ZrhABBRooWsyNAkI6brRECBBkpWsyOAEE6brZGCBBkpGgxOwIE6bjZGiFAkJGixewIEKTjZmuEAEFGihazI0CQjputEQIEGSlazI4AQTputkYIEGSkaDE7AgTpuNkaIUCQkaLF7AgQpONma4QAQUaKFrMjQJCQ28vLy4fJ5+fnDx/f+vnkGKdrfL72j91/cf3kjP/bDEGCRj/fmH/64+AIHwS4Jufp34+eL7n+6gxBguaP3nBH54Mj/By59gQ5er2vnkTpWVbmCFI0fesNeb5/+vvDw8Pd+/t79Bp36RXrfPHo+QoEMysEOVj1pa++R2/IS/M/BHl6err6lPjqCfKVHJf2PEHy0gmSs/qrrzjn32Rfe0L87sZuX8EOIJgbJUhYefITpFu+Sb5VkFvOFyKYHCNIUPvRr9y3vHIdudFPR7/1fAGC2RGCBNV//h3DaeX8Vaj9PcT53uPj493b29uvE6X//584X4BhcoQgk7ULnRIgSErK3CQBgkzWLnRKgCApKXOTBAgyWbvQKQGCpKTMTRIgyGTtQqcECJKSMjdJgCCTtQudEiBISsrcJAGCTNYudEqAICkpc5MECDJZu9ApAYKkpMxNEiDIZO1CpwQIkpIyN0mAIJO1C50SIEhKytwkAYJM1i50SoAgKSlzkwQIMlm70CkBgqSkzE0SIMhk7UKnBAiSkjI3SYAgk7ULnRIgSErK3CQBgkzWLnRKgCApKXOTBAgyWbvQKQGCpKTMTRIgyGTtQqcECJKSMjdJgCCTtQudEiBISsrcJAGCTNYudEqAICkpc5MECDJZu9ApAYKkpMxNEiDIZO1CpwQIkpIyN0mAIJO1C50SIEhKytwkAYJM1i50SoAgKSlzkwQIMlm70CkBgqSkzE0SIMhk7UKnBAiSkjI3SYAgk7ULnRIgSErK3CQBgkzWLnRKgCApKXOTBAgyWbvQKQGCpKTMTRIgyGTtQqcECJKSMjdJ4Duy69HPzC8E/AAAAABJRU5ErkJggg==">
                    <h3>News &amp; Events</h3>
                    <ul class="list-unstyled">
                        <li>
                            <dl>
                                <dt>热烈庆祝北美线路开通</dt>
                                <dd>热烈庆祝北美线路开通热烈庆祝北美线路开.... <a href="#">[Read more]</a></dd>
                            </dl>
                        </li>
                        <li>
                            <dl>
                                <dt>热烈庆祝北美线路开通</dt>
                                <dd>热烈庆祝北美线路开通热烈庆祝北美线路开.... <a href="#">[Read more]</a></dd>
                            </dl>
                        </li>
                        <li>
                            <dl>
                                <dt>热烈庆祝北美线路开通</dt>
                                <dd>热烈庆祝北美线路开通热烈庆祝北美线路开.... <a href="#">[Read more]</a></dd>
                            </dl>
                        </li>
                    </ul>
                </div>

            </div>


        </div>
    </div>
    
    

<script type="text/javascript">
    $(function() {
        $(window).resize(function() {
            $("#main-container").css("min-height", $(window).height() - 383);
        }).resize();
    })
</script>
	<!-- /主体 -->

	<!-- 底部 -->
	
    <!-- 底部
    ================================================== -->
    
<div class="footer">
  <div class="container">
    <p class="text-muted"> <p> 本程序 <strong><a href="http://www.onethink.cn" target="_blank">AdmExpress</a></strong> 强力驱动 <?php echo C('WEB_SITE_ICP');?></p></p>
  </div>
</div>

<script type="text/javascript">
(function(){
	var ThinkPHP = window.Think = {
		"ROOT"   : "/admexp", //当前网站地址
		"APP"    : "/admexp/index.php", //当前项目地址
		"PUBLIC" : "/admexp/Public", //项目公共目录地址
		"DEEP"   : "<?php echo C('URL_PATHINFO_DEPR');?>", //PATHINFO分割符
		"MODEL"  : ["<?php echo C('URL_MODEL');?>", "<?php echo C('URL_CASE_INSENSITIVE');?>", "<?php echo C('URL_HTML_SUFFIX');?>"],
		"VAR"    : ["<?php echo C('VAR_MODULE');?>", "<?php echo C('VAR_CONTROLLER');?>", "<?php echo C('VAR_ACTION');?>"]
	}
})();
</script>
 <!-- 用于加载js代码 -->
<!-- 页面footer钩子，一般用于加载插件JS文件和JS代码 -->
<?php echo hook('pageFooter', 'widget');?>
<div class="hidden"><!-- 用于加载统计代码等隐藏元素 -->
	
</div>

	<!-- /底部 -->
</body>
</html>