<?php

// +----------------------------------------------------------------------
// | ADMEXPRESS [ 国际领先的转运系统 ]
// +----------------------------------------------------------------------
// | Copyright (c) 2014 扬州安蝶梦电子商务有限公司 All rights reserved.
// +----------------------------------------------------------------------
// | Author: 曹梦龙 <138888611@qq.com> <http://www.andiemeng.com>
// +----------------------------------------------------------------------


namespace Admin\Model;
use Think\Model\ViewModel;
class OrdersViewModel extends ViewModel {
   public $viewFields = array(       
     'Orders'=>array('*','status'),
     'Orders_detailexpress'=>array('id'=>'odid','*', '_on'=>'Orders.id=Orders_detailexpress.oid'), 
   );
 }
