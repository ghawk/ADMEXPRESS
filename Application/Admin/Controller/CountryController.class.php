<?php
// +----------------------------------------------------------------------
// | ADMEXPRESS [ 国际领先的转运系统 ]
// +----------------------------------------------------------------------
// | Copyright (c) 2014 扬州安蝶梦电子商务有限公司 All rights reserved.
// +----------------------------------------------------------------------
// | Author: 曹梦龙 <138888611@qq.com> <http://www.andiemeng.com>
// +----------------------------------------------------------------------
namespace Admin\Controller;

/**
 * 后台国家控制器
 * @author 曹梦龙 <138888611@qq.com>
 */
class CountryController extends AdminController{
    /**
     * 国家管理首页
     * @author 曹梦龙 <138888611@qq.com>
     */
    public function index(){      
        $name  =   I('name');        
        $map['name_chinese']=   array('like', '%'.(string)$name.'%');        
        $list   = $this->lists('common_country', $map);
        int_to_string($list);
        $this->assign('_list', $list);
        $this->meta_title = '国家信息';
        $this->display();
    }
     /**
     * 国家状态修改
     * @author 曹梦龙 <138888611@qq.com>
     */
    public function changeStatus($method=null,$id){
        $id = is_array($id) ? implode(',',$id) : $id;
        if ( empty($id) ) {
            $this->error('请选择要操作的数据!');
        }       
        $map['country_id'] =   array('in',$id);
        switch ( strtolower($method) ){
            case 'forbidcountry':
                $this->forbid('common_country', $map );
                break;
            case 'resumecountry':
                $this->resume('common_country', $map );
                break;
            case 'deletecountry':
                $this->delete('common_country', $map );
                break;
            default:
                $this->error('参数非法');
        }
    } 
}
