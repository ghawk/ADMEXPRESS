<?php

// +----------------------------------------------------------------------
// | ADMEXPRESS [ 国际领先的转运系统 ]
// +----------------------------------------------------------------------
// | Copyright (c) 2014 扬州安蝶梦电子商务有限公司 All rights reserved.
// +----------------------------------------------------------------------
// | Author: 曹梦龙 <138888611@qq.com> <http://www.andiemeng.com>
// +----------------------------------------------------------------------

namespace Admin\Controller;

/**
 * 后台包裹控制器
 * @author 曹梦龙 <138888611@qq.com>
 */
class OrderController extends AdminController {

    /**
     * 订单管理界面
     * @author 曹梦龙 <138888611@qq.com>
     */
    public function index($type = -1) {
        
        $uid = get_user_id($name);        

        $name = I('name');       
   
        $id = I('id');
        if ($uid) {
            $map['uid'] = $uid;
        }
        if ($id) {
            $map['id'] = $id;
        }

        if ($type > -1) {
            $map['status'] = $type;
        }
        $list = $this->lists('OrdersView', $map);

        foreach ($list AS $k => $v) {
            $list[$k]['packages'] = M('package')->where(array('oid' => $v['id']))->select();
        }
        int_to_string_package($list);
        $this->assign('_list', $list);
        $this->type = $type;
        $this->meta_title = '订单打包计费操作';
        $this->display();
    }

    /**
     * 订单详情界面
     * @author 曹梦龙 <138888611@qq.com>
     */
    public function details($oid = null) {

        if (IS_POST) {
            
        } else {
            for ($i = 0; $i < count($oid); $i++) {
                $map['oid'] = $oid;
                $data[$i]['orderdata'] = D('OrdersView')->where($map)->find();
                $warehouse_id = $data[$i]['orderdata']['wid'];
                $map2['id'] = $warehouse_id;
                $data[$i]['warehousedata'] = D('Warehouse')->where($map2)->find();
                $map3['oid'] = $oid;
                $data[$i]['packagelist'] = D('Package')->where($map3)->select();     
              
            }
            $this->assign("oid", I('oid'));
            $this->assign("data", $data);
            $map['oid'] = $oid;
            $list = D('OrdersView')->where($map)->select();
            int_to_string_package($list); 
            $this->order = $list[0];
            $mapl['fid'] = $oid;
            $mapl['tpye'] = 1;
            $logs = D('logs')->where($mapl)->select();
            $this->logs = $logs;
            $this->display();
        }
    }

    /**
     * 订单打包计费操作
     * @author 曹梦龙 <138888611@qq.com>
     */
    public function pack() {
        $map['status'] = 3000;
        $list = $this->lists('OrdersView', $map);
        foreach ($list AS $k => $v) {
            $list[$k]['packages'] = M('package')->where(array('oid' => $v['id']))->select();
        }
        int_to_string_package($list);
        $this->assign('_list', $list);
        $this->meta_title = '订单打包计费操作';
        $this->display();
    }

    /**
     * 出库单打印操作
     * @author 曹梦龙 <138888611@qq.com>
     */
    public function packprint($oid = null) {

        if (!is_array($oid)) {
            $oid = array($oid);
        }

        for ($i = 0; $i < count($oid); $i++) {
            $map['oid'] = $oid[$i];
            $data[$i]['orderdata'] = D('OrdersView')->where($map)->find();
            $warehouse_id = $data[$i]['orderdata']['wid'];
            $map2['id'] = $warehouse_id;
            $data[$i]['warehousedata'] = D('Warehouse')->where($map2)->find();
            $map3['oid'] = $oid[$i];
            $data[$i]['packagelist'] = D('Package')->where($map3)->select();
        }
        $this->assign("data", $data);
        $this->display();
    }

    /**
     * 打包计费操作
     * @author 曹梦龙 <138888611@qq.com>
     */
    public function packaction($oid = null) {

        if (IS_POST) {
            $oid = I('oid');
            $weight = I('weight');
            $bulkfactor = I('bulkfactor');
            $location = I('location');
            if (!$weight) {
                if (!$bulkfactor) {
                    $this->error("包裹出库打包计费操作失败！");
                }
            }

            $jisuanweight = 0;
            if ($bulkfactor > $weight) {
                $jisuanweight = $bulkfactor;
            } else {
                $jisuanweight = $weight;
            }

            $orderview = D('OrdersView')->where(array("id" => $oid))->find();

            $expressline = M('expressline')->where(array("eid" => $eid))->find();

            if ($jisuanweight < $expressline['fweight']) {
                //小于首重
                $money = $expressline['fprice'];
            } else {
                //运费计算
                $money = $expressline['fprice'] + ($jisuanweight - $expressline['fweight']) / $expressline['cweight'] * $expressline['cprice'];
            }

            if ($orderview['status'] == 3000) {
                //修改订单状态
                $Ordersdata['status'] = 4000;
                $Ordersdata['money'] = $money;
                D('Orders')->where(array('id' => $oid))->save($Ordersdata);
                //修改订单详情状态                
                $OrderExpsdata['weight'] = $weight;
                $OrderExpsdata['bulkfactor'] = $bulkfactor;
                $OrderExpsdata['location'] = $location;

                D('orders_detailexpress')->where(array('id' => $oid))->save($OrderExpsdata);
                //修改包裹状态
                D('package')->where(array('oid' => $oid))->save($Ordersdata);
                $log = array('fid' => $oid, 'tpye' => 1, 'info' => '4000', 'remark' => '订单已经打包成功，等待用户付款。', 'username' => session('user_auth.username'), 'createdate' => NOW_TIME);
                M('logs')->add($log);
                $this->success('包裹出库打包成功，运费已经计算完毕，等待用户付款！', U('pack'));
            } else {
                $this->error("包裹出库打包计费操作失败2！");
            }
        } else {
            for ($i = 0; $i < count($oid); $i++) {
                $map['oid'] = $oid;
                $data[$i]['orderdata'] = D('OrdersView')->where($map)->find();
                $warehouse_id = $data[$i]['orderdata']['wid'];
                $map2['id'] = $warehouse_id;
                $data[$i]['warehousedata'] = D('Warehouse')->where($map2)->find();
                $map3['oid'] = $oid;
                $data[$i]['packagelist'] = D('Package')->where($map3)->select();
            }
            $this->assign("oid", I('oid'));
            $this->assign("data", $data);
            $this->display();
        }
    }

    /**
     * 订单物流发货
     * @author 曹梦龙 <138888611@qq.com>
     */
    public function output() {
        $map['status'] = 5000;
        $list = $this->lists('OrdersView', $map);
        foreach ($list AS $k => $v) {
            $list[$k]['packages'] = M('package')->where(array('oid' => $v['id']))->select();
        }
        int_to_string_package($list);
        $this->assign('_list', $list);

        $this->meta_title = '物流发货操作';
        $this->display();
    }

    /* 
     * 订单物流发货操作
     * @author 曹梦龙 <138888611@qq.com>
     */

    public function outputaction($oid = null) {

        if (IS_POST) {
            $oid = I('oid');
            $expname = I('expname');
            $expnumber = I('expnumber');
            if (!$expname) {
                if (!$expnumber) {
                    $this->error("订单发货操作失败！");
                }
            }
            $orderview = D('OrdersView')->where(array("id" => $oid))->find();
            if ($orderview['status'] == 5000) {
                //修改订单状态
                $Ordersdata['status'] = 6000;
                D('Orders')->where(array('id' => $oid))->save($Ordersdata);
                //修改订单详情状态                
                $OrderExpsdata['expname'] = $expname;
                $OrderExpsdata['expnumber'] = $expnumber;
                D('orders_detailexpress')->where(array('id' => $oid))->save($OrderExpsdata);
                //修改包裹状态
                D('package')->where(array('oid' => $oid))->save($Ordersdata);
                $log = array('fid' => $oid, 'tpye' => 1, 'info' => '6000', 'remark' => '订单已经发货，请耐心等待签收。物流公司：' . $expname . " 物流跟踪码：" . $expnumber, 'username' => session('user_auth.username'), 'createdate' => NOW_TIME);
                M('logs')->add($log);
                $this->success('订单发货完毕，等待用户签收！', U('output'));
            } else {
                $this->error("订单发货操作失败！");
            }
        } else {
            for ($i = 0; $i < count($oid); $i++) {
                $map['oid'] = $oid;
                $data[$i]['orderdata'] = D('OrdersView')->where($map)->find();
                $warehouse_id = $data[$i]['orderdata']['wid'];
                $map2['id'] = $warehouse_id;
                $data[$i]['warehousedata'] = D('Warehouse')->where($map2)->find();
                $map3['oid'] = $oid;
                $data[$i]['packagelist'] = D('Package')->where($map3)->select();
                //dump($data[$i]['packagelist']);
                // $map4['poid'] = $oid[$i];
                //  $data[$i]['serverlist'] = D('servicelist')->where($map4)->select();
            }
            $this->assign("oid", I('oid'));
            $this->assign("data", $data);
            $this->assign("eid", $data[0]['orderdata']['eid']);

            $this->display();
        }
    }

    /*
     * 订单价格修改操作
     * @author 曹梦龙 <138888611@qq.com>
     */

    public function paymodfiyaction($oid = null) {
        $money = I('money');
        $remark = I('remark');
        if (!$money || !$remark) {
            $this->error("订单金额修改失败！");
        }
        $map['id'] = $oid;
        $order = D('orders')->where($map)->find();


        if ($order['status'] = 4000) {
            $mapd['id'] = $oid;
            $data['money'] = $money;           
            if (!D('orders')->where($mapd)->save($data)) {
                $this->error("订单金额修改失败！");
            } else {
                $log = array('fid' => $oid, 'tpye' => 1, 'info' => '4000', 'remark' => '订单修改了金额，等待用户付款。修改理由：'.$remark, 'username' => session('user_auth.username'), 'createdate' => NOW_TIME);
                M('logs')->add($log);
                $this->success('订单金额修改成功！', U('details', array('oid' => $oid)));
            }
        }
    }

    /*
     * 订单重量修改操作
     * @author 曹梦龙 <138888611@qq.com>
     */

    public function weightmodfiyaction($oid = null) {

        $weight = I('weight');
        $bulkfactor = I('bulkfactor');
        if (!$weight && !$bulkfactor) {
            $this->error("订单重量修改失败！");
        }
        $map['id'] = $oid;
        $order = D('orders')->where($map)->find();

        if ($order['status'] = 4000) {
            $mapd['oid'] = $oid;
            $data['weight'] = $weight;
            $data['bulkfactor'] = $bulkfactor;
            if (!D('orders_detailexpress')->where($mapd)->save($data)) {
                $this->error("订单重量修改失败！");
            } else {
                $log = array('fid' => $oid, 'tpye' => 1, 'info' => '4000', 'remark' => '订单修改了包裹重量。', 'username' => session('user_auth.username'), 'createdate' => NOW_TIME);
                M('logs')->add($log);
                $this->success('订单重量修改成功！', U('details', array('oid' => $oid)));
            }
        }
    }

    /*
     * 订单物流修改操作
     * @author 曹梦龙 <138888611@qq.com>
     */

    public function outputmodfiyaction($oid = null) {

        $expname = I('expname');
        $expnumber = I('expnumber');

        if (!$expname || !$expnumber) {
            $this->error("物流信息修改失败！");
        }


        $map['id'] = $oid;
        $order = D('orders')->where($map)->find();

        if ($order['status'] = 6000) {
            $mapd['oid'] = $oid;
            $data['expname'] = $expname;
            $data['expnumber'] = $expnumber;
            if (!D('orders_detailexpress')->where($mapd)->save($data)) {
                $this->error("物流信息修改失败！");
            } else {
                $log = array('fid' => $oid, 'tpye' => 1, 'info' => '6000', 'remark' => '订单修改了物流信息，请耐心等待签收。物流公司：' . $expname . " 物流跟踪码：" . $expnumber, 'username' => session('user_auth.username'), 'createdate' => NOW_TIME);
                M('logs')->add($log);
                $this->success('物流信息修改成功！', U('details', array('oid' => $oid)));
            }
        }
    }

}
