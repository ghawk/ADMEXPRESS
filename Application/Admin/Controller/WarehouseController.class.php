<?php

// +----------------------------------------------------------------------
// | ADMEXPRESS [ 国际领先的转运系统 ]
// +----------------------------------------------------------------------
// | Copyright (c) 2014 扬州安蝶梦电子商务有限公司 All rights reserved.
// +----------------------------------------------------------------------
// | Author: 曹梦龙 <138888611@qq.com> <http://www.andiemeng.com>
// +----------------------------------------------------------------------

namespace Admin\Controller;

/**
 * 后台仓库控制器
 * @author 曹梦龙 <138888611@qq.com>
 */
class WarehouseController extends AdminController {

    /**
     * 仓库管理首页
     * @author 曹梦龙 <138888611@qq.com>
     */
    public function index() {
        $name = I('name');
        $map['name'] = array('like', '%' . (string) $name . '%');
        $list = $this->lists('WarehouseView', $map);
        int_to_string($list);
        $this->assign('_list', $list);
        $this->meta_title = '仓库信息';
        $this->display();
    }

    /**
     * 仓库状态修改
     * @author 曹梦龙 <138888611@qq.com>
     */
    public function changeStatus($method = null, $id) {
        $id = is_array($id) ? implode(',', $id) : $id;
        if (empty($id)) {
            $this->error('请选择要操作的数据!');
        }
        $map['id'] = array('in', $id);
        switch (strtolower($method)) {
            case 'forbid':
                $this->forbid('Warehouse', $map);
                break;
            case 'resume':
                $this->resume('Warehouse', $map);
                break;
            case 'delete':
                $this->delete('Warehouse', $map);
                break;
            default:
                $this->error('参数非法');
        }
    }

    /**
     * 仓库添加
     * @author 曹梦龙 <138888611@qq.com>
     */
    public function add($country_id = '', $name = '', $country = '', $province = '', $city = '', $location = '', $zipcode = '', $person = '', $phone = '', $remark = '') {

        if (IS_POST) {
            $warehouse = array('country_id' => $country_id, 'name' => $name, 'country' => $country, 'province' => $province, 'city' => $city, 'location' => $location, 'zipcode' => $zipcode, 'person' => $person, 'phone' => $phone, 'remark' => $remark, 'status' => 0, 'createdate' => NOW_TIME);
            if (!M('warehouse')->add($warehouse)) {
                $this->error("仓库添加失败！");
            } else {
                $this->success('仓库添加成功！', U('index'));
            }
        } else {
            $countrylist = M('common_country')->where('status=1')->select();
            $this->countrylist = $countrylist;
            $this->display();
        }
    }

    public function edit( $country_id = '', $name = '', $country = '', $province = '', $city = '', $location = '', $zipcode = '', $person = '', $phone = '', $remark = '') {
        $id=I('id');
        
        if (IS_POST) {
            $warehouse = array('country_id' => $country_id, 'name' => $name, 'country' => $country, 'province' => $province, 'city' => $city, 'location' => $location, 'zipcode' => $zipcode, 'person' => $person, 'phone' => $phone, 'remark' => $remark, 'modifydate' => NOW_TIME);
           
            if (!D('warehouse')->where(array('id' => $id))->save($warehouse)) {
                $this->error("仓库修改失败！");
            } else {
                $this->success('仓库修改成功！', U('index'));
            }
        } else {
            $countrylist = M('common_country')->where('status=1')->select();
            $data = M('warehouse')->where(array('id' => $id))->find();
            $this->countrylist = $countrylist;
            $this->wdata = $data;
            $this->display();
        }
    }

}
