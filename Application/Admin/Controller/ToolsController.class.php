<?php

// +----------------------------------------------------------------------
// | ADMEXPRESS [ 国际领先的转运系统 ]
// +----------------------------------------------------------------------
// | Copyright (c) 2014 扬州安蝶梦电子商务有限公司 All rights reserved.
// +----------------------------------------------------------------------
// | Author: 曹梦龙 <138888611@qq.com> <http://www.andiemeng.com>
// +----------------------------------------------------------------------

namespace Admin\Controller;

class ToolsController extends AdminController {

    public function index() {
        if (IS_POST) {
            $data = I('rmark');
            $type=I('type');
             $arraydata=explode(',',strtolower($this->trimall($data))); 
            switch (strtolower($type)) {
                 case '0':
                      $this->data = $this->trimall(strtolower(str_replace(',', '="",$', $data)));
                     break;
                 case '1':
                     $data='';
                     for($index=0;$index<count($arraydata);$index++){ 
                 
                        $data=$data."'".$arraydata[$index]."'=>$".$arraydata[$index].',';
                     }

                     $this->data = $data;
                     break;              
                 default:
                     $this->error('参数非法');
             }
           
            $this->display();
        } else {
            $this->display();
        }
    }

    public function trimall($str) {//删除空格
        $qian = array(" ", "　", "\t", "\n", "\r");
        $hou = array("", "", "", "", "");
        return str_replace($qian, $hou, $str);
    }

}
