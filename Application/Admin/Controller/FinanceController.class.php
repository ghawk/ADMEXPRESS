<?php

// +----------------------------------------------------------------------
// | ADMEXPRESS [ 国际领先的转运系统 ]
// +----------------------------------------------------------------------
// | Copyright (c) 2014 扬州安蝶梦电子商务有限公司 All rights reserved.
// +----------------------------------------------------------------------
// | Author: 曹梦龙 <138888611@qq.com> <http://www.andiemeng.com>
// +----------------------------------------------------------------------

namespace Admin\Controller;

/**
 * 后台财务控制器
 * @author 曹梦龙 <138888611@qq.com>
 */
class FinanceController extends AdminController {

    /**
     * 银行账号管理首页
     * @author 曹梦龙 <138888611@qq.com>
     */
    public function banklist() {
        $name = I('name');
        $map['name'] = array('like', '%' . (string) $name . '%');
        $list = $this->lists('company_bank', $map);
        int_to_string($list);
        $this->assign('_list', $list);
        $this->meta_title = '银行信息';
        $this->display();
    }

    /**
     * 仓库状态修改
     * @author 曹梦龙 <138888611@qq.com>
     */
    public function changeStatus($method = null, $id) {
        $id = is_array($id) ? implode(',', $id) : $id;
        if (empty($id)) {
            $this->error('请选择要操作的数据!');
        }
        $map['id'] = array('in', $id);
        switch (strtolower($method)) {
            case 'forbidbank':
                $this->forbid('company_bank', $map);
                break;
            case 'resumebank':
                $this->resume('company_bank', $map);
                break;
            case 'deletebank':
                $this->delete('company_bank', $map);
                break;
            default:
                $this->error('参数非法');
        }
    }

    /**
     * 银行添加
     * @author 曹梦龙 <138888611@qq.com>
     */
    public function bankadd($name = "", $mold = "", $accountnumber = "", $accountname = "", $accountbalance = "") {

        if (IS_POST) {
            $bank = array('name' => $name, 'mold' => $mold, 'accountnumber' => $accountnumber, 'accountname' => $accountname, 'accountbalance' => $accountbalance, 'createdate' => NOW_TIME);
            if (!M('company_bank')->add($bank)) {
                $this->error("银行添加失败！");
            } else {
                $this->success('银行添加成功！', U('banklist'));
            }
        } else {
            $this->display();
        }
    }

    /**
     * 银行修改
     * @author 曹梦龙 <138888611@qq.com>
     */
    public function bankedit($name = "", $mold = "", $accountnumber = "", $accountname = "", $accountbalance = "") {
        $id = I('id');
        if (IS_POST) {
            $bank = array('name' => $name, 'mold' => $mold, 'accountnumber' => $accountnumber, 'accountname' => $accountname, 'accountbalance' => $accountbalance, 'modifydate' => NOW_TIME);
            if (!D('company_bank')->where(array('id' => $id))->save($bank)) {
                $this->error("银行修改失败！");
            } else {
                $this->success('银行修改成功！', U('banklist'));
            }
        } else {
            $data = M('company_bank')->where(array('id' => $id))->find();
            $this->wdata = $data;
            $this->display();
        }
    }

    /**
     * 银行充值记录管理首页
     * @author 曹梦龙 <138888611@qq.com>
     */
    public function rechargelist() {
        $username = I('name');
        $uid = get_user_id($username);
        $map['uid'] = array('like', '%' . (string) $uid . '%');
        if ($username) {
            $list = $this->lists('company_bankdetail', $map);
        } else {
            $list = $this->lists('company_bankdetail');
        }
        int_to_string($list);
        $this->assign('_list', $list);
        $this->meta_title = '充值记录';
        $this->display();
    }

    /**
     * 银行现金提现页
     * @author 曹梦龙 <138888611@qq.com>
     */
    public function takecash() {
        if (IS_POST) {
            $bid = I('bid');
            $money = I('money');
            $remark = I('remark');
            if ($money <= 0) {
                $this->error('提现金额不正确！');
            }

            $model = new \Think\Model();
            $model->startTrans();
            $flag = false;
            $result = $model->table(C('DB_PREFIX') . 'company_bank')->where(array('id' => $bid))->setDec('accountbalance', $money); //银行资金减少！
            if ($result) {
                //
                dump($result);
                $data['bid'] = $bid;
                $data['mold'] = 2; //支持
                $data['money'] = $money;
                $data['remark'] = session('user_auth.username') . '添加了提现操作记录：提现了' . $money . '元。 ' . I('remark');
                $data['operator'] = session('user_auth.username');
                $data['CreateDate'] = NOW_TIME;
                $result = $model->table(C('DB_PREFIX') . 'company_bankdetail')->add($data);
                if ($result) {
                    dump($result);
                    $flag = true;
                    $model->commit();
                }
            }
            if (!$result) {
                $model->rollback();
            }
            $this->success("提现记录添加成功！", U('rechargelist/?r=19'));
        } else {
            $banklist = D('company_bank')->select();
            $this->banklist = $banklist;
            $this->display();
        }
    }

    /**
     * 银行手动充值页
     * @author 曹梦龙 <138888611@qq.com>
     */
    public function rechargeadd($username = '') {
        if (IS_POST) {
            $member = D('ucenter_member')->where(array("username" => $username))->find();
            if (is_null($member)) {
                $this->error('账户不存在，充值失败！');
            }

            $uid = $member['id'];            
            $member2 = D('member')->where(array('uid' => $uid))->find();
            $balance=$member2['balance'];
            
            $bid = I('bid');
            $money = I('money');
            $remark = I('remark');
            if ($money <= 0) {
                $this->error('充值金额不正确！');
            }

            $model = new \Think\Model();
            $model->startTrans();
            $flag = false;

            $result = $model->table(C('DB_PREFIX') . 'member')->where(array('uid' => $uid))->setInc('balance', $money); //用户资金增加！
            if ($result) {
                $result = $model->table(C('DB_PREFIX') . 'company_bank')->where(array('id' => $bid))->setInc('accountbalance', $money); //银行资金增加！
                if ($result) {
                    //
                    $data['bid'] = $bid;
                    $data['uid'] = $uid;
                    $data['mold'] = 1;
                    $data['money'] = $money;
                    $data['remark'] = session('user_auth.username') . '手动给用户账户：' . $username . ' 充值了' . $money . '元 ' . I('remark');
                    $data['operator'] = session('user_auth.username');
                    $data['CreateDate'] = NOW_TIME;
                    $result = $model->table(C('DB_PREFIX') . 'company_bankdetail')->add($data);
                    if ($result) {
                        $flag = true;
                        $model->commit(); 
                        $nowbalance = $balance + $money;
                        $paymentdetail = array('uid' => $uid, 'mold' => 1, 'money' => $money, 'createdate' => NOW_TIME, 'remark' => "[充值]进行了手动充值", 'operator' => session('user_auth.username'), 'balance' => $nowbalance);
                        M('member_paymentdetail')->add($paymentdetail);
                    }
                }
            }
            if (!$result) {
                $model->rollback();
            }
            $this->success("充值成功！", U('rechargelist/?r=19'));
        } else {
            $banklist = D('company_bank')->select();
            $this->banklist = $banklist;
            $this->display();
        }
    }

}
