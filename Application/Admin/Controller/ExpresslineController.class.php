<?php

// +----------------------------------------------------------------------
// | ADMEXPRESS [ 国际领先的转运系统 ]
// +----------------------------------------------------------------------
// | Copyright (c) 2014 扬州安蝶梦电子商务有限公司 All rights reserved.
// +----------------------------------------------------------------------
// | Author: 曹梦龙 <138888611@qq.com> <http://www.andiemeng.com>
// +----------------------------------------------------------------------

namespace Admin\Controller;

/**
 * 后台物流控制器
 * @author 曹梦龙 <138888611@qq.com>
 */
class ExpresslineController extends AdminController {

    /**
     * 线路管理首页
     * @author 曹梦龙 <138888611@qq.com>
     */
    public function index() {
        $name = I('name');
        $map['name'] = array('like', '%' . (string) $name . '%');
        $list = $this->lists('Expressline', $map);
        int_to_string($list);
        $this->assign('_list', $list);
        $this->meta_title = '线路信息';
        $this->display();
    }

    /**
     * 线路状态修改
     * @author 曹梦龙 <138888611@qq.com>
     */
    public function changeStatus($method = null, $id) {
        $id = is_array($id) ? implode(',', $id) : $id;
        if (empty($id)) {
            $this->error('请选择要操作的数据!');
        }
        $map['id'] = array('in', $id);
        switch (strtolower($method)) {
            case 'forbid':
                $this->forbid('Expressline', $map);
                break;
            case 'resume':
                $this->resume('Expressline', $map);
                break;
            case 'delete':
                $this->delete('Expressline', $map);
                break;
            default:
                $this->error('参数非法');
        }
    }

    /**
     * 线路添加
     * @author 曹梦龙 <138888611@qq.com>
     */
    public function add($name = "", $sendcountryid = "", $acceptcountryid = "", $expname = "",  $expurl = "",  $fweight = "", $fprice = "", $cweight = "", $cprice = "", $remark = "") {

        if (IS_POST) {
            $expressline = array('name' => $name, 'sendcountryid' => $sendcountryid, 'acceptcountryid' => $acceptcountryid,'expname'=>$expname,'expurl'=>$expurl,'fweight' => $fweight, 'fprice' => $fprice, 'cweight' => $cweight, 'cprice' => $cprice, 'remark' => $remark, 'status' => 0, 'createdate' => NOW_TIME);
            if (!M('expressline')->add($expressline)) {
                $this->error("线路添加失败！");
            } else {
                $this->success('线路添加成功！', U('index'));
            }
        } else {
            $countrylist = M('common_country')->where('status=1')->select();
            $this->countrylist = $countrylist;
            $countrylist2 = M('common_country')->select();
            $this->countrylist2 = $countrylist2;
            $this->display();
        }
    }

    public function edit($name = "", $sendcountryid = "", $acceptcountryid = "",  $expname = "",  $expurl = "", $fweight = "", $fprice = "", $cweight = "", $cprice = "", $remark = "") {
        $id = I('id');
        if (IS_POST) {
            $expressline = array('name' => $name, 'sendcountryid' => $sendcountryid, 'acceptcountryid' => $acceptcountryid,'expname'=>$expname,'expurl'=>$expurl, 'fweight' => $fweight, 'fprice' => $fprice, 'cweight' => $cweight, 'cprice' => $cprice, 'remark' => $remark, 'modifydate' => NOW_TIME);
            if (!D('expressline')->where(array('id' => $id))->save($expressline)) {
                $this->error("线路修改失败！");
            } else {
                $this->success('线路修改成功！', U('index'));
            }
        } else {
            $countrylist = M('common_country')->where('status=1')->select();
            $data = M('expressline')->where(array('id' => $id))->find();
            $this->countrylist = $countrylist;
            $countrylist2 = M('common_country')->select();
            $this->countrylist2 = $countrylist2;
            $this->wdata = $data;
            $this->display();
        }
    }

}
