<?php

// +----------------------------------------------------------------------
// | ADMEXPRESS [ 国际领先的转运系统 ]
// +----------------------------------------------------------------------
// | Copyright (c) 2014 扬州安蝶梦电子商务有限公司 All rights reserved.
// +----------------------------------------------------------------------
// | Author: 曹梦龙 <138888611@qq.com> <http://www.andiemeng.com>
// +----------------------------------------------------------------------

namespace Admin\Controller;

/**
 * 后台包裹控制器
 * @author 曹梦龙 <138888611@qq.com>
 */
class PackageController extends AdminController {

    /**
     * 包裹管理首页
     * @author 曹梦龙 <138888611@qq.com>
     */
    public function index() {
        $name = I('name');
        $uid = get_user_id($name);
        $map['uid'] = $uid;
        $list = $this->lists('PackageView', $map);
        int_to_string_package($list);
        //dump($list);
        $this->assign('_list', $list);
        $this->meta_title = '仓库包裹信息';
        $this->display();
    }

    /**
     * 包裹状态修改
     * @author 曹梦龙 <138888611@qq.com>
     */
    public function changeStatus($method = null, $id) {
        $id = is_array($id) ? implode(',', $id) : $id;
        if (empty($id)) {
            $this->error('请选择要操作的数据!');
        }
        $map['id'] = array('in', $id);
        switch (strtolower($method)) {
            case 'narrived':
                $this->narrivedaction($id);
                break;
            case 'resume':
                break;
            default:
                $this->error('参数非法');
        }
    }

    /**
     * 包裹签收首页
     * @author 曹梦龙 <138888611@qq.com>
     */
    public function narrived() {
        $name = I('name');
        $uid = get_user_id($name);
        $map['uid'] = $uid;
        $map['status'] = 0;
        $list = $this->lists('PackageView', $map);
        int_to_string_package($list);
        //dump($list);
        $this->assign('_list', $list);
        $this->meta_title = '仓库包裹签收信息';
        $this->display();
    }

    /**
     * 包裹签收操作
     * @author 曹梦龙 <138888611@qq.com>
     */
    public function narrivedaction($pid) {
        $data['status'] = 1000;
        $result = M('Package')->where(array('id' => $pid))->save($data);
        if (!$result) {
            $this->error("包裹签收操作失败！");
        } else {
            $username = session('user_auth.username');
            $log = array('fid' => $pid, 'tpye' => 0, 'info' => '1000', 'remark' => '包裹抵达仓库,已经签收！', 'username' => $username, 'createdate' => NOW_TIME);
            M('logs')->add($log);
            $this->success('包裹签收操作成功！', U('narrived'));
        }
    }

    /**
     * 包裹入库首页
     * @author 曹梦龙 <138888611@qq.com>
     */
    public function putin() {
        $name = I('name');
        $uid = get_user_id($name);
        $map['uid'] = $uid;
        $map['status'] = 1000;
        $list = $this->lists('PackageView', $map);
        int_to_string_package($list);
        //dump($list);
        $this->assign('_list', $list);
        $this->meta_title = '仓库包裹签收信息';
        $this->display();
    }

    /**
     * 包裹入库操作
     * @author 曹梦龙 <138888611@qq.com>
     */
    public function putinaction() {
        if (IS_POST) {
            $pid = I('pid');
            $data['status'] = 2000;
            $data['location'] = I('location');
            $data['weight'] = I('weight');
            $data['bulkfactor'] = I('bulkfactor');
            $result = M('Package')->where(array('id' => $pid))->save($data);
            if (!$result) {
                $this->error("包裹入库操作失败！");
            } else {
                $username = session('user_auth.username');
                $log = array('fid' => $pid, 'tpye' => 0, 'info' => '2000', 'remark' => '包裹已经入库！', 'username' => $username, 'createdate' => NOW_TIME);
                M('logs')->add($log);
                $this->success('包裹入库操作成功！', U('putin'));
            }
        } else {
            $pid = I('pid');
            $package = M('Package')->where(array('id' => $pid))->find();
            $this->wdata = $package;
            $this->meta_title = '仓库包裹入库操作';
            $this->display();
        }
    }

}
