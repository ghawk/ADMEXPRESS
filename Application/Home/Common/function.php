<?php
// +----------------------------------------------------------------------
// | OneThink [ WE CAN DO IT JUST THINK IT ]
// +----------------------------------------------------------------------
// | Copyright (c) 2013 http://www.onethink.cn All rights reserved.
// +----------------------------------------------------------------------
// | Author: 麦当苗儿 <zuojiazi@vip.qq.com> <http://www.zjzit.cn>
// +----------------------------------------------------------------------

/**
 * 前台公共库文件
 * 主要定义前台公共函数库
 */

/**
 * 检测验证码
 * @param  integer $id 验证码ID
 * @return boolean     检测结果
 * @author 麦当苗儿 <zuojiazi@vip.qq.com>
 */
function check_verify($code, $id = 1){
	$verify = new \Think\Verify();
	return $verify->check($code, $id);
}

/**
 * 获取列表总行数
 * @param  string  $category 分类ID
 * @param  integer $status   数据状态
 * @author 麦当苗儿 <zuojiazi@vip.qq.com>
 */
function get_list_count($category, $status = 1){
    static $count;
    if(!isset($count[$category])){
        $count[$category] = D('Document')->listCount($category, $status);
    }
    return $count[$category];
}

/**
 * 获取段落总数
 * @param  string $id 文档ID
 * @return integer    段落总数
 * @author 麦当苗儿 <zuojiazi@vip.qq.com>
 */
function get_part_count($id){
    static $count;
    if(!isset($count[$id])){
        $count[$id] = D('Document')->partCount($id);
    }
    return $count[$id];
}

/**
 * 获取导航URL
 * @param  string $url 导航URL
 * @return string      解析或的url
 * @author 麦当苗儿 <zuojiazi@vip.qq.com>
 */
function get_nav_url($url){
    switch ($url) {
        case 'http://' === substr($url, 0, 7):
        case '#' === substr($url, 0, 1):
            break;        
        default:
            $url = U($url);
            break;
    }
    return $url;
}

/**
 * select返回的数组进行整数映射转换
 *
 * @param array $map  映射关系二维数组  array(
 *                                          '字段名1'=>array(映射关系数组),
 *                                          '字段名2'=>array(映射关系数组),
 *                                           ......
 *                                       )
 * @author 朱亚杰 <zhuyajie@topthink.net>
 * @return array
 *
 *  array(
 *      array('id'=>1,'title'=>'标题','status'=>'1','status_text'=>'正常')
 *      ....
 *  )
 *
 */
function int_to_string(&$data,$map=array('status'=>array(1=>'正常',-1=>'删除',0=>'禁用',2=>'未审核',3=>'草稿'))) {
    if($data === false || $data === null ){
        return $data;
    }
    $data = (array)$data;
    foreach ($data as $key => $row){
        foreach ($map as $col=>$pair){
            if(isset($row[$col]) && isset($pair[$row[$col]])){
                $data[$key][$col.'_text'] = $pair[$row[$col]];
            }
        }
    }
    return $data;
}


//创建TOKEN
function creatToken() {
    $code = chr(mt_rand(0xB0, 0xF7)) . chr(mt_rand(0xA1, 0xFE)) . chr(mt_rand(0xB0, 0xF7)) . chr(mt_rand(0xA1, 0xFE)) . chr(mt_rand(0xB0, 0xF7)) . chr(mt_rand(0xA1, 0xFE));
    session('TOKEN', authcode($code));
}

//判断TOKEN
function checkToken($token) {
    if ($token == session('TOKEN')) {
        session('TOKEN', NULL);
        return TRUE;
    } else {
        return FALSE;
    }
}

/* 加密TOKEN */
function authcode($str) {
    $key = "ANDIAMON";
    $str = substr(md5($str), 8, 10);
    return md5($key . $str);
}

/* 当前时间 */
function Now() {
    return date("Y-m-d H:i:s");
}

/***
 * 包裹状态数字转字符串
 * @author 曹梦龙 <138888611@qq.com>
 */
function int_to_string_package(&$data,$map=array('status'=>array(0=>'预报入库，等待签收操作',1000=>'已签收，等待入库操作',2000=>'已经入库',3000=>'出库打包中',4000=>'已经打包，等待用户付款',5000=>'已经付款，等待出库操作',6000=>'已经出库，等待抵达',7000=>'用户已经签收'))) {
    int_to_string($data, $map);
}

/***
 * 获取国家名称
 * @author 曹梦龙 <138888611@qq.com>
 */
function get_country_name($countryid){
    $country=M('common_country')->where(array('country_id'=>$countryid))->find();
    return $country['name_chinese'];
}

/***
 * 获取用户余额
 * @author 曹梦龙 <138888611@qq.com>
 */
function get_member_balance($uid){
    $member=M('member')->where(array('uid'=>$uid))->find();
    return $member['balance'];
}

/***
 * 统计在仓库中的包裹
 * @author 曹梦龙 <138888611@qq.com>
 */
function get_warehousepackage_count($uid){
    $map['uid']=$uid;
    $map['status']=array('not in','6000,7000,0');
    $count=M('package')->where($map)->count();        
    return $count;
}

        

/***
 * 统计订单
 * @author 曹梦龙 <138888611@qq.com>
 */
function get_orderstatus_count($uid,$status){
    $map['uid']=$uid;
     $map['status']=$status;
    $count=M('orders')->where($map)->count();        
    return $count;
}
