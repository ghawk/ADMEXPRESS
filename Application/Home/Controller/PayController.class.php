<?php

// +----------------------------------------------------------------------
// | ADMEXPRESS [ 国际领先的转运系统 ]
// +----------------------------------------------------------------------
// | Copyright (c) 2014 扬州安蝶梦电子商务有限公司 All rights reserved.
// +----------------------------------------------------------------------
// | Author: 曹梦龙 <138888611@qq.com> <http://www.andiemeng.com>
// +----------------------------------------------------------------------

namespace Home\Controller;

use Think\Controller;
use Org\Alipay;
class PayController extends HomeController{
 
    //充值页面
    public function recharge(){
        $this->nvazhi=4;
        if (IS_POST) {   
            
            	$alipayp['total_fee'] = I('post.money');//订单总金额
		$alipayp['out_trade_no'] = $this->createOrderNo();//商户订单ID
		$alipayp['subject'] = session('user_auth.username')."充值".I('post.money').'元';//订单商品标题
		$alipayp['body'] = '用户'. session('user_auth.username')."充值".I('post.money').'元';;//订单商品描述
		$alipayp['show_url'] = '';//订单商品地址
                //创建充值记录
                $paylog = array('uid' => session('user_auth.uid'), 'out_trade_no' => $alipayp['out_trade_no'], 'money' =>$alipayp['total_fee'] , 'create_time' => NOW_TIME);
                $payid = M('pay')->add($paylog);
                
		$alipay = new Alipay\Alipay();
		$alipay->toAlipay($alipayp);
                
                
                
                
        } else {
            //在此之前goods1的业务订单已经生成，状态为等待支付
            $this->display();
        }
    }
    
     
    /**
     * 生成订单号
     * 可根据自身的业务需求更改
     */
    public function createOrderNo() {
        $year_code = array('A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J');
        return $year_code[intval(date('Y')) - 2010] .
                strtoupper(dechex(date('m'))) . date('d') .
                substr(time(), -5) . substr(microtime(), 2, 5) . sprintf('d', rand(0, 99));
    }
    
    /***
     * 支付成功测试
     */
      public function pay1() {
          echo '1111';
      }
      
      public function pay2() {
          echo '2222';
      }
    
}
