<?php

namespace Home\Controller;

use Think\Controller;

/**
 * 支付宝
 * --------------------
 * @author 曹梦龙
 * 2014/11/10 14:12
 * ------------------
 */
class AlipayController extends Controller {
   protected function _initialize(){
        /* 读取站点配置 */
        $config = api('Config/lists');
        C($config); //添加配置        
    }
    public function BuildAlipayConfig() {
        //↓↓↓↓↓↓↓↓↓↓请在这里配置您的基本信息↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓
        //合作身份者id，以2088开头的16位纯数字

       // $alipay_config['partner'] = '2088901499723503';
        //安全检验码，以数字和字母组成的32位字符
       // $alipay_config['key'] = 'slt7b1vrqz5s8s2hlgfc6l9nlg7widfr';
       // $alipay_config['alipay_seller_email'] = 'admpay@andiamon.com';
        $alipay_config=C("ALIPAY_CONFIG");
        //↑↑↑↑↑↑↑↑↑↑请在这里配置您的基本信息↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑
        //签名方式 不需修改
        $alipay_config['sign_type'] = strtoupper('MD5');
        //字符编码格式 目前支持 gbk 或 utf-8
        $alipay_config['input_charset'] = strtolower('utf-8');
        //ca证书路径地址，用于curl中ssl校验
        //请保证cacert.pem文件在当前文件夹目录中
        $alipay_config['cacert'] = getcwd() . '\\cacert.pem';
        //访问模式,根据自己的服务器是否支持ssl访问，若支持请选择https；若不支持请选择http
        $alipay_config['transport'] = 'http';
        
        return $alipay_config;
    }

    /**
     * 支付宝支付过程
     */
    public function PayAction() {
 
         $alipay_config=$this->BuildAlipayConfig();
         dump($alipay_config);
        //↓↓↓↓↓↓↓↓↓↓请在这里配置您的基本信息↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓
        //合作身份者id，以2088开头的16位纯数字
        //支付类型
        $payment_type = "1";
        //必填，不能修改
        //服务器异步通知页面路径

        $notify_url = U('Home/Alipay/alipayNotify', '', true, true);
        //需http://格式的完整路径，不能加?id=123这类自定义参数  "http://v1.admexpress.cn/alipay/log.php"; 
        //页面跳转同步通知页面路径
        $return_url = U('Home/Alipay/alipayReturn', '', true, true);
        //需http://格式的完整路径，不能加?id=123这类自定义参数，不能写成http://localhost/
        //卖家支付宝帐户
        $seller_email = $alipay_config['alipay_seller_email'];
        //必填
        //商户订单号
        $out_trade_no = $this->createOrderNo();
        //商户网站订单系统中唯一订单号，必填
        //订单名称
        $subject = session('user_auth.username') . "充值" . I('post.money') . '元'; //订单商品标题;
        //必填
        //付款金额
        $total_fee = I('post.money');
        //必填
        //订单描述

        $body = '用户' . session('user_auth.username') . "充值" . I('post.money') . '元';
        //订单商品描述
        //商品展示地址
        //$show_url = $_POST['WIDshow_url'];
        //需以http://开头的完整路径，例如：http://www.商户网址.com/myorder.html
        //防钓鱼时间戳
        $anti_phishing_key = "";
        //若要使用请调用类文件submit中的query_timestamp函数
        //客户端的IP地址
        $exter_invoke_ip = "";
        //非局域网的外网IP地址，如：221.0.0.1
        //构造要请求的参数数组，无需改动
        $parameter = array(
            "service" => "create_direct_pay_by_user",
            "partner" => trim($alipay_config['partner']),
            "payment_type" => $payment_type,
            "notify_url" => $notify_url,
            "return_url" => $return_url,
            "seller_email" => $seller_email,
            "out_trade_no" => $out_trade_no,
            "subject" => $subject,
            "total_fee" => $total_fee,
            "body" => $body,
            "show_url" => $show_url,
            "anti_phishing_key" => $anti_phishing_key,
            "exter_invoke_ip" => $exter_invoke_ip,
            "_input_charset" => trim(strtolower($alipay_config['input_charset']))
        );

        //创建充值记录
        $paylog = array('uid' => session('user_auth.uid'), 'out_trade_no' => $out_trade_no, 'money' => $total_fee, 'create_time' => NOW_TIME);
        $payid = M('pay')->add($paylog);
      
        //建立请求
        $alipaySubmit = new \Org\Alipay\AlipaySubmit($alipay_config);
        $html_text = $alipaySubmit->buildRequestForm($parameter, "get", "确认");        
 
        echo $html_text;
    }

    /**
     * 支付宝同步通知
     * @return [type] [description]
     */
    public function alipayReturn() {
         $alipay_config = $this->BuildAlipayConfig();
        //计算得出通知验证结果
        $alipayNotify = new \Org\Alipay\AlipayNotify($alipay_config);
        $verify_result = $alipayNotify->verifyReturn();
        if ($verify_result) {//验证成功
            /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            //请在这里加上商户的业务逻辑程序代码
            //——请根据您的业务逻辑来编写程序（以下代码仅作参考）——
            //获取支付宝的通知返回参数，可参考技术文档中页面跳转同步通知参数列表
            //商户订单号
            $out_trade_no = $_GET['out_trade_no'];

            //支付宝交易号

            $trade_no = $_GET['trade_no'];

            //交易状态
            $trade_status = $_GET['trade_status'];


            if ($_GET['trade_status'] == 'TRADE_FINISHED' || $_GET['trade_status'] == 'TRADE_SUCCESS') {
                //判断该笔订单是否在商户网站中已经做过处理
                //如果没有做过处理，根据订单号（out_trade_no）在商户网站的订单系统中查到该笔订单的详细，并执行商户的业务程序
                //如果有做过处理，不执行商户的业务程序
                   $this->choongzhi($out_trade_no);
                $this->success("恭喜您！在线充值成功！", U('Center/accountdetail'));
            } else {
                echo "trade_status=" . $_GET['trade_status'];
            }

           // echo "验证成功<br />";

            //——请根据您的业务逻辑来编写程序（以上代码仅作参考）——
            /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        } else {
            //验证失败
            //如要调试，请看alipay_notify.php页面的verifyReturn函数
            echo "验证失败";
        }
    }

    /**
     * 支付宝异步通知
     * @return [type] [description]
     */
    public function alipayNotify() {
        $alipay_config = $this->BuildAlipayConfig();

        //计算得出通知验证结果
        $alipayNotify = new \Org\Alipay\AlipayNotify($alipay_config);
        $verify_result = $alipayNotify->verifyNotify();

        if ($verify_result) {//验证成功
            /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            //请在这里加上商户的业务逻辑程序代
            //——请根据您的业务逻辑来编写程序（以下代码仅作参考）——
            //获取支付宝的通知返回参数，可参考技术文档中服务器异步通知参数列表
            //商户订单号
            $out_trade_no = $_POST['out_trade_no'];

            //支付宝交易号

            $trade_no = $_POST['trade_no'];

            //交易状态
            $trade_status = $_POST['trade_status'];


            if ($_POST['trade_status'] == 'TRADE_FINISHED' || $_POST['trade_status'] == 'TRADE_SUCCESS') {
                //判断该笔订单是否在商户网站中已经做过处理
                //如果没有做过处理，根据订单号（out_trade_no）在商户网站的订单系统中查到该笔订单的详细，并执行商户的业务程序
                //如果有做过处理，不执行商户的业务程序
                //注意：
                //该种交易状态只在两种情况下出现
                //1、开通了普通即时到账，买家付款成功后。
                //2、开通了高级即时到账，从该笔交易成功时间算起，过了签约时的可退款时限（如：三个月以内可退款、一年以内可退款等）后。
                //调试用，写文本函数记录程序运行情况是否正常
                //logResult("这里写入想要调试的代码变量值，或其他运行的结果记录");
                $this->choongzhi($out_trade_no);
               // logResult('alipayNotify');
            }

            //——请根据您的业务逻辑来编写程序（以上代码仅作参考）——

            echo "success";  //请不要修改或删除
            /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        } else {
            //验证失败
            echo "fail";

            //调试用，写文本函数记录程序运行情况是否正常
            //logResult("这里写入想要调试的代码变量值，或其他运行的结果记录");
        }
    }

    /*     * *
     * 付款过程
     */

    public function choongzhi($order_id) {
        $mappay['out_trade_no'] = $order_id;
        $paylog = M('pay')->where($mappay)->find();
        $uid = $paylog['uid'];
        $member2 = D('member')->where(array('uid' => $uid))->find();
        $balance = $member2['balance'];

        $bid = C('alipay_BankID');
        $money = $paylog['money'];
        $remark = "支付宝在线充值";
        if ($money <= 0) {
            $this->error('充值金额不正确！');
        }

        $model = new \Think\Model();
        $model->startTrans();
        $flag = false;

        $result = $model->table(C('DB_PREFIX') . 'member')->where(array('uid' => $uid))->setInc('balance', $money); //用户资金增加！
        if ($result) {
            $result = $model->table(C('DB_PREFIX') . 'company_bank')->where(array('id' => $bid))->setInc('accountbalance', $money); //银行资金增加！
            if ($result) {
                //
                $data['bid'] = $bid;
                $data['uid'] = $uid;
                $data['mold'] = 1;
                $data['money'] = $money;
                $data['remark'] = session('user_auth.username') . '用户：' . $username . ' 在线通过支付宝充值了' . $money . '元 ' . I('remark');
                $data['operator'] = session('user_auth.username');
                $data['CreateDate'] = NOW_TIME;
                $result = $model->table(C('DB_PREFIX') . 'company_bankdetail')->add($data);
                if ($result) {
                    $flag = true;
                    $model->commit();
                    $nowbalance = $balance + $money;
                    $paymentdetail = array('uid' => $uid, 'mold' => 1, 'money' => $money, 'createdate' => NOW_TIME, 'remark' => "[充值]在线通过支付宝充值", 'operator' => session('user_auth.username'), 'balance' => $nowbalance);
                    M('member_paymentdetail')->add($paymentdetail);
                }
            }
        }
        if (!$result) {
            $model->rollback();
        }
       // echo "success";
    }

    /**
     * 生成订单号
     * 可根据自身的业务需求更改
     */
    public function createOrderNo() {
        $year_code = array('A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J');
        return $year_code[intval(date('Y')) - 2010] .
                strtoupper(dechex(date('m'))) . date('d') .
                substr(time(), -5) . substr(microtime(), 2, 5) . sprintf('d', rand(0, 99));
    }

}
